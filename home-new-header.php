<!DOCTYPE html >
<html <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
	<link rel="icon" href="<?php bloginfo('template_directory'); ?>/favicon.png" type="image/x-icon" />
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="google-site-verification" content="tywopIqpjkjV5jSDfFz7ZyrLEeMvhZKTHUbgag2htiE" />
	<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/print.css" type="text/css" media="print" />
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700|Roboto:300,400,500,700|Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-52534617-1', 'auto');
  ga('send', 'pageview');
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-52534617-1']);
  _gaq.push(['_trackPageview']);
</script>
<meta name="google-site-verification" content="tywopIqpjkjV5jSDfFz7ZyrLEeMvhZKTHUbgag2htiE" />
<!--Start of Zopim Live Chat Script
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){
z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?4BJvipvC5a4HDnVfR1s9DQPrStkoSqSs';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
 End of Zopim Live Chat Script-->
 <script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0051/4340.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/slick.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/flexslider.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/new-home.css?ver=0.5">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W9ZS3JF');</script>
<!-- End Google Tag Manager -->
</head>
<body class="homepage new-home-page">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W9ZS3JF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php include ('Mobile-Detect/Mobile_Detect.php');
//echo get_template_directory_uri();
$detect = new Mobile_Detect;
?>
<div class="right_contant_new lg-sdhelp-cover">
<a href="<?php bloginfo('url'); ?>/contact/" class="contt_h lg-sdhelp" >Talk to Us</a>
<a href="<?php bloginfo('url'); ?>/blog/" class="latest_h lg-sdhelp" >Latest News</a>
</div>
<!--........desktop version................-->
<div class="bg_outer">
<div class="mid_cont">
<div class="top_head desktop-header">
<div class="logo"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="Logo" /></a></div>
<div class="right_head">
<div class="florida_cont" style='position:relative;'>
<h2 style='position:absolute;top:-57px;right:-10px;background:#FE680D;color:white;padding:4px 10px;border:1px solid #FE680D;border-radius:4px;display:none;'><a style='font-size:14px;color:white;text-shadow:0 0 2px rgba(50,50,50,.9);' href='/march-2015-seminar'>Negligent Security Seminar | March 2015</a></h2>
<h2>Florida personal injury lawyers </h2>
<h3>talk to an attorney  <span><a href="tel:8889881774">888.988.1774</a></span></h3>
</div>
<div class="header_nav_container" >
<div class="nav-area">
<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
</div>
<!-- <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?> -->
</div>
</div>
</div>
<?php include( "php/social-links.php" ); ?>
<div class="clear"></div>
</div>
<?php $wp_query = new WP_Query('page_id=7242');
	while($wp_query->have_posts()) : $wp_query->the_post(); ?>
    <?php
		$rows = get_field('slider');
		if($rows)
		{
			echo '<div class="banner-slider-home mobile-slider"><div class="banner-slider-mobile">';

			foreach($rows as $row)
			{ 	?>
            		<div><div class="home-slid"><img src="<?php echo $row['image_bg'];?>" /></div></div>
			<?php }

			echo '</div></div>';
		}
	?>
    <?php endwhile;?>
<div class="clear"></div>
<div class="new_slider_wrap">
    <div class="new_slider_text">
        <h2>Because sometimes bad things just <br /> happen to good people.</h2>
        <!-- <p>CONSTRUCTION ACCIDENTS • MOTOR VEHICLE ACCIDENTS • NURSING HOME ABUSE & NEGLECT • BOATING & CRUISE SHIP ACCIDENTS • MEDICAL MALPRACTICE • BRAIN INJURIES • NEGLIGENT SECURITY AND VIOLENT CRIME • CATASTROPHIC INJURIES • AMUSEMENT & THEME PARK ACCIDENTS • AVIATION ACCIDENTS</p> -->
        <style type="text/css">
            .email_submit{
                /*overflow: hidden;*/
            }
            .email_submit .hidden{
                display: none;
            }
            .email_submit .visible{

            }
            .error{
                color: #cf2626;
                padding: 5px;
            }
            .process{
                width: 684px;
                float: left;
            }
            .place-holder{
                color: #fff;
                position: absolute;
                top: 24%;
                font-family: "Oswald",sans-serif;
                font-size: 18px;
                font-weight: 300;
                /*bottom: 8%;*/
                margin-bottom: 0;
                margin-top: 0;
                left: 10%;
            }
        </style>
        <form class="email_submit_frm" autocomplete="off">
        	<div class="email_submit">
                <input type="text" name="case-type" class="email_bar case-type visible" readonly />
                <h3 class="letter-animate case-type-place place-holder visible">Please choose your case type &or;</h3>
                <div class="case-type-options hidden">
                    <ul>
                        <li>CONSTRUCTION ACCIDENTS</li>
                        <li>MOTOR VEHICLE ACCIDENTS</li>
                        <li>NURSING HOME ABUSE & NEGLECT</li>
                        <li>BOATING & CRUISE SHIP ACCIDENTS</li>
                        <li>MEDICAL MALPRACTICE</li>
                        <li>BRAIN INJURIES</li>
                        <li>NEGLIGENT SECURITY AND VIOLENT CRIME</li>
                        <li>CATASTROPHIC INJURIES</li>
                        <li>AMUSEMENT & THEME PARK ACCIDENTS</li>
                        <li>AVIATION ACCIDENTS</li>
                        <li>OTHER</li>
                    </ul>
                </div>
                <input type="text" name="phone" class="email_bar phone hidden" />
            	<h3 class="letter-animate phone-place place-holder hidden">What is the best number to reach you at?</h3>
                <input type="text" name="emial" class="email_bar emial hidden" />
                <h3 class="letter-animate email-place place-holder hidden">What is your email address?</h3>
                <input type="text" name="fname" class="email_bar fname hidden" />
                <h3 class="letter-animate fname-place place-holder hidden">What is your name?</h3>
                <input type="submit" name="submit" class="submit_bar hidden1" value="Next" />
            </div>
            <div class="error"></div>
        </form>
    </div>
</div>
<div class="custom-otr-banner-column" style="margin:0 auto; position:relative">
<div class="miama_wrap">
<div class="mim_wrap">
<div class="left_mim">
        <img src="<?php bloginfo('template_directory'); ?>/images/new-office-1.png" alt="Office" />
        <div class="clear"></div>
        <h2>Miami Office</h2>
        <p>1401 Brickell Avenue, Suite 900<br />
        Miami, FL 33131</p>
  <p>Toll Free: <a href="tel:8889881774">888.988.1774</a><br>
Phone: <a href="tel:3057484121">305.748.4121</a><br>
Fax: <a href="#">305.675.0123</a></p>
<a class="left_mim_link" target="_blank" href="https://www.google.com/maps/place/1401+Brickell+Ave+%23900/@25.7600611,-80.1918411,17z/data=!3m1!4b1!4m2!3m1!1s0x88d9b680dfd0913f:0xad57e84687bfe30b">VIEW MAP</a>
    <!-- <div class="eql-btn">
        <a class="left_mim_link" href="<?php bloginfo('url');?>/contact">Contact Us</a>
    </div> -->
</div>
<div class="mid_mim">
<img src="<?php bloginfo('template_directory'); ?>/images/new-office-1.png" alt="Office" />
<div class="clear"></div>
<h2>Orlando Office</h2>
<p>121 South Orange Avenue<br />
Suite 1270, Orlando, FL 32801</p>
<p>Toll Free: <a href="tel:8889881774">888.988.1774</a><br>
Phone: <a href="tel:4073474986">407.347.4986</a><br><a>&nbsp;</a></p>
<a class="mid_mim_link" target="_blank" href="https://www.google.com/maps/place/121+S+Orange+Ave+%231270/@28.541108,-81.378705,17z/data=!3m1!4b1!4m2!3m1!1s0x88e77afe775c318f:0x66e1b4bb8afbd4ca">VIEW MAP</a>
<!-- <div class="eql-btn">
<a class="mid_mim_link" href="<?php bloginfo('url');?>/contact">Contact Us</a>
</div> -->
</div>
<div class="right_mim_con">
        <img src="<?php bloginfo('template_directory'); ?>/images/new-consult-1.png" alt="Office" />
        <div class="clear"></div>
        <h2>get a free consultation</h2>
    <!-- <div class="eql-btn">
        <a class="mid_mim_link" href="<?php bloginfo('url');?>/contact">Contact Us</a>
    </div> -->
<?php  echo do_shortcode('[contact-form-7 id="3178" title="Consultation"]'); ?>
<!--<form action="" method="post">
<input type="text" value="Name" name="" class="textfeild" onfocus="if (this.value==this.defaultValue) this.value = ''"
onblur="if (this.value=='') this.value = this.defaultValue" />
<input type="text" value="Phone" name="" class="textfeild" onfocus="if (this.value==this.defaultValue) this.value = ''"
onblur="if (this.value=='') this.value = this.defaultValue" />
<input type="text" value="Email" name="" class="textfeild" onfocus="if (this.value==this.defaultValue) this.value = ''"
onblur="if (this.value=='') this.value = this.defaultValue" />
<input type="submit" value="Submit" name="" class="right_mim_con_btn" />
</form>-->
</div>
</div>
</div>
</div>
<?php $wp_query = new WP_Query('page_id=7242');
	while($wp_query->have_posts()) : $wp_query->the_post(); ?>
    <?php
		$rows = get_field('slider');
		if($rows)
		{
			echo '<div class="banner-slider-home"><div class="banner-slider"><div class="flexslider"><ul class="slides">';
			$x = 1;
			foreach($rows as $row)
			{ 	if($row['video_id'] != ''){
				?>
            		<li>
                    	<div class="videoWrap">
                        	<video id="video_<?php echo $x; ?>"  onended=resumeslider() preload="none" width="100%" height="100%" poster="<?php echo $row['image_bg'];?>" autoplay loop muted>
								<source src="<?php echo $row['video_id'];?>.mp4" type='video/mp4' />
								<source src="<?php echo $row['video_id'];?>.webm" type='video/webm' />
							</video>

                        </div>
                     </li>
                <?php } else{ ?>
                    <li><div class=" "><img src="<?php echo $row['image_bg'];?>" /></div></li>
                <?php }
                $x++;
                }
			echo '</ul></div></div></div>';
		}
	?>
    <?php endwhile;?>
<div class="clear"></div>
</div>
