<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
	<link rel="icon" href="<?php bloginfo('template_directory'); ?>/favicon.png" type="image/x-icon" />
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="google-site-verification" content="tywopIqpjkjV5jSDfFz7ZyrLEeMvhZKTHUbgag2htiE" />
	<title><?php bloginfo('name'); ?> <?php if (is_single()) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/print.css" type="text/css" media="print" />
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700|Roboto:300,400,500,700|Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if (is_singular()) {
    	wp_enqueue_script('comment-reply');
	} ?>
	<?php wp_head(); ?>
	<script>
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
		ga('create', 'UA-52534617-1', 'auto');
		ga('send', 'pageview');
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-52534617-1']);
		_gaq.push(['_trackPageview']);
	</script>
	<meta name="google-site-verification" content="tywopIqpjkjV5jSDfFz7ZyrLEeMvhZKTHUbgag2htiE" />
	<!--Start of Zopim Live Chat Script-->
	<!--<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){
	z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
	$.src='//v2.zopim.com/?4BJvipvC5a4HDnVfR1s9DQPrStkoSqSs';z.t=+new Date;$.
	type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
	</script>-->
 	<!--End of Zopim Live Chat Script-->
	<script type="text/javascript">
		setTimeout(function() {
			var a = document.createElement("script");
			var b = document.getElementsByTagName("script")[0];
			a.src = document.location.protocol + "//script.crazyegg.com/pages/scripts/0051/4340.js?" + Math.floor(new Date().getTime() / 3600000);
			a.async = true;
			a.type = "text/javascript";
			b.parentNode.insertBefore(a, b)
		}, 1);
	</script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/slick.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/flexslider.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/new-home.css?ver=0.5">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-W9ZS3JF');
	</script>
	<!-- End Google Tag Manager -->
</head>

<body <?php echo body_class('new-home-page continue');?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W9ZS3JF" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php include('Mobile-Detect/Mobile_Detect.php');
	//echo get_template_directory_uri();
	$detect = new Mobile_Detect;
	?>
	<div class="right_contant_new lg-sdhelp-cover">
		<a href="<?php bloginfo('url'); ?>/contact/" class="contt_h lg-sdhelp">Talk to Us</a>
		<a href="<?php bloginfo('url'); ?>/blog/" class="latest_h lg-sdhelp">Latest News</a>
	</div>
	<!--........desktop version................-->
	<div class="bg_outer">
		<div class="mid_cont">
			<div class="top_head desktop-header">
				<div class="logo"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="Logo" /></a></div>
				<div class="right_head">
					<div class="florida_cont" style='position:relative;'>
						<h2 style='position:absolute;top:-57px;right:-10px;background:#FE680D;color:white;padding:4px 10px;border:1px solid #FE680D;border-radius:4px;display:none;'><a
								style='font-size:14px;color:white;text-shadow:0 0 2px rgba(50,50,50,.9);' href='/march-2015-seminar'>Negligent Security Seminar | March 2015</a></h2>
						<h2>Florida personal injury lawyers </h2>
						<h3>talk to an attorney <span><a href="tel:8889881774">888.988.1774</a></span></h3>
					</div>
					<div class="header_nav_container">
						<div class="nav-area">
							<?php wp_nav_menu(array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' )); ?>
						</div>
						<!-- <?php wp_nav_menu(array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' )); ?> -->
					</div>
				</div>
			</div>
			<?php include("php/social-links.php"); ?>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
