<?php
	if ( function_exists( "register_sidebar" ) ) {

		register_sidebar(

			array(
				"name" => __( "Right Hand Page Sidebar" ),
				"id" => "sidebar-page",
				"description" => __( "Widgets in this area will be shown on the right-hand side of pages." ),
				"before_widget" => "<div class=\"sidebar-page\">",
				"after_widget" => "</div>",
			)

		);

		register_sidebar(

			array(

				"name" => __( "Right Hand Category Sidebar" ),

				"id" => "sidebar-category",

				"description" => __( "Widgets in this area will be shown on the right-hand side of categories." ),

				"before_widget" => "<div class=\"category-page\">",

				"after_widget" => "</div>",

			)

		);

                register_sidebar(

            array(

                "name" => __( "Sidebar Location" ),

                "id" => "sidebar-location",

                "description" => __( "Widgets in this area will be shown on the right-hand side of Location." ),

                "before_widget" => "<div class=\"location-page\">",

                "after_widget" => "</div>",

            )

        );

	}

	register_nav_menus(

		array(

			"primary" => __( "Primary Navigation", "leighton" ),

		)

	);

	register_nav_menus(

		array(

			"secondary" => __( "Secondary Navigation", "leighton" ),

		)

	);

if ( function_exists('register_sidebar') )

    register_sidebar(array(

		'name' => __('Sidebar2', 'lessons-for-us'),

        'before_widget' => '	<div>',

		'after_widget' => '		</div>',



));



if ( function_exists('register_sidebar') )

    register_sidebar(array(

		'name' => __('Search', 'search-bar'),

        'before_widget' => '	<div>',

        'before_title' => '		<h2>',

        'after_title' => '		</h2>',

		'after_widget' => '		</div>',



));



add_theme_support( 'post-thumbnails' );



function get_attachment_id_from_src ($image_src) {



		global $wpdb;

		$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";

		$id = $wpdb->get_var($query);

		return $id;



}



function wp_get_attachment( $attachment_id ) {



$attachment = get_post( $attachment_id );

return array(

	'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),

	'caption' => $attachment->post_excerpt,

	'description' => $attachment->post_content,

	'href' => get_permalink( $attachment->ID ),

	'src' => $attachment->guid,

	'title' => $attachment->post_title

);

}



if ( ! function_exists( 'twentytwelve_content_nav' ) ) :

function twentytwelve_content_nav( $html_id ) {

	global $wp_query;



	if ( $wp_query->max_num_pages > 1 ) : ?>

		<nav id="<?php echo esc_attr( $html_id ); ?>" class="navigation" role="navigation">

			<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentytwelve' ); ?></h3>

			<div class="clearfix main-pagin">

			<div class="nav-previous"><p><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentytwelve' ) ); ?></p></div>

			<div class="nav-next"><p><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentytwelve' ) ); ?></p></div>

			</div>

		</nav><!-- .navigation -->

	<?php endif;

}

endif;



function my_enqueue() {

    wp_enqueue_style( 'animate-css', get_template_directory_uri().'/css/animate.css' );

    wp_enqueue_script( 'fittext', get_template_directory_uri() . '/textillate-master/assets/jquery.fittext.js', array('jquery') );

    wp_enqueue_script( 'lettering', get_template_directory_uri() . '/textillate-master/assets/jquery.lettering.js', array('jquery') );

    wp_enqueue_script( 'textillate', get_template_directory_uri() . '/textillate-master/jquery.textillate.js', array('jquery') );
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css');
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.js', array('jquery') );
	wp_enqueue_script( 'logo', get_template_directory_uri() . '/js/logo.js', array('jquery') );
	wp_enqueue_script( 'dropdown', get_template_directory_uri() . '/js/dropdown.js', array('jquery') );
	wp_enqueue_script( 'general', get_template_directory_uri() . '/js/general.js', array('jquery') );

    wp_localize_script( 'general', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

}

 add_action( 'wp_enqueue_scripts', 'my_enqueue' );



function sendmail_front() {

    $email = $phone = $fname = $case_type = '';

    if(isset($_POST['email'])){

       $email = $_POST['email'];

    }



    if(isset($_POST['phone'])){

       $phone = $_POST['phone'];

    }



    if(isset($_POST['fname'])){

       $fname = $_POST['fname'];

    }



    if(isset($_POST['case_type'])){

       $case_type = $_POST['case_type'];

    }

    if ( function_exists( 'ot_get_option' ) ) {

		// $to = ot_get_option( 'c_email_to' );
		$to = 'mit2sumit@gmail.com';

		$subject = ot_get_option( 'c_email_subject' );

		$headers = "From: <Hello@leightonlaw.com>\r\n";

		$headers .= "MIME-Version: 1.0\r\n";

		$headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";

		$c_bcc = ot_get_option( 'c_bcc' );

		if($c_bcc != ''){

			$c_bcc = 'Bcc:'.$c_bcc."\r\n";

			$headers .= $c_bcc;

		}



		$body='

		<html xmlns="http://www.w3.org/1999/xhtml">

		<head>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		</head>

		<body>

		<strong>Contact Details</strong><hr />

		   <table align="center" width="99%" border="0" cellpadding="0" cellspacing="0">

			 <tr><td width="30%" style="padding:7px 5px; background:#cccccc; font-weight:bold;">Name:</td><td style="padding:7px 5px;background:#cccccc;">'.$fname.'</td></tr>

			 <tr><td width="30%" style="padding:7px 5px; font-weight:bold;">Phone:</td><td style="padding:7px 5px;">'.$phone.'</td></tr>

			 <tr><td width="30%" style="padding:7px 5px; background:#cccccc; font-weight:bold;">Email:</td><td style="padding:7px 5px;background:#cccccc;">'.$email.'</td></tr>

			 <tr><td width="30%" style="padding:7px 5px; font-weight:bold;">Case type:</td><td style="padding:7px 5px;">'.$case_type.'</td></tr>

		   </table>

		</body>

		</html>';

		$check = wp_mail( $to, $subject, $body, $headers, "-fHello@leightonlaw.com" );



		if($check){

			echo json_encode( array("AJAX" => "Mail Sent Success") );

		}

		else{

			echo json_encode( array("AJAX" => "Mail Sent Not Success") );

		}

    }

    exit();

    //return $check;

}

add_action('wp_ajax_sendmail_front', 'sendmail_front' );

add_action('wp_ajax_nopriv_sendmail_front', 'sendmail_front' );



add_action( 'widgets_init', 'theme_slug_widgets_init' );

function theme_slug_widgets_init() {

    register_sidebar( array(

        'name' => __( 'Avvo logo code', 'theme-slug' ),

        'id' => 'sidebar-12',

        'description' => __( 'Widgets in this area will be shown using shortcode', 'theme-slug' ),

        'before_widget' => '',

		'after_widget'  => '',

		'before_title'  => '',

		'after_title'   => '',

    ) );

}



function wpbeginner_numeric_posts_nav() {
    if( is_singular() )
        return;
    global $wp_query;
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
    echo '<div class="navigation"><ul>' . "\n";
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';

    }



    /** Link to current page, plus 2 pages in either direction if necessary */

    sort( $links );

    foreach ( (array) $links as $link ) {

        $class = $paged == $link ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );

    }



    /** Link to last page, plus ellipses if necessary */

    if ( ! in_array( $max, $links ) ) {

        if ( ! in_array( $max - 1, $links ) )

            echo '<li>…</li>' . "\n";



        $class = $paged == $max ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );

    }



    /** Next Post Link */

    if ( get_next_posts_link() )

        printf( '<li>%s</li>' . "\n", get_next_posts_link() );



    echo '</ul></div>' . "\n";



}

function cus_menu_add(){

global $post;

$args = array(

		'post_type' => 'post',

		'posts_per_page' => 6,

		'post_status' => 'publish',

		'category_name' => 'practice-areas',

        'orderby'=>'title',

		);

$post_array = get_posts($args);

$menu_name = 'New Menu';

$name_of_menu_item_to_append_to = 'Practice Areas';

$id_of_menu_item_to_append_to =  get_wp_object_id( $name_of_menu_item_to_append_to, 'nav_menu_item' );

foreach ($post_array as $post_data) {

	$title = $post_data->post_title;

	$id = $post_data->ID;

	$content = $post_data->post_content;

    $url = get_permalink($id);

	$image = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'large' );

        	$data = '<div class="post-image" style="background-image:url('.$image[0].')">



        			</div>

            		<div class="post-item-name">

                		<h2>'.$title.'</h2>

                		<p>' .wp_trim_words($content, $num_words = 20, $more = null).'</p>

            		</div>

                    <a href='.$url.' class="read-link">

                    <div class="read-more">Read More

                    </div>

                    </a>

                    ';

    	$new_submenu_item = array(

		    'text' => $data,

		    'url' => $url

		);

		add_subitems_to_menu(

		    $menu_name,

		    array( $new_submenu_item )

		);

}

wp_reset_query();

}

add_action('wp_head', 'cus_menu_add');

function add_subitems_to_menu( $menu_name, $subitems ) {

	if ( is_admin() ) {

        return ;

   }

	add_filter( 'wp_nav_menu_objects', function( $items, $menu )

        use( $menu_name, $subitems ) {

  		$parent_menu_item_id = 3513;

		$menu_order = 5624+ 1;

        if($menu->theme_location =="primary" ){

            foreach ( $subitems as $key => $subitem ) {

                $items[] = (object) array(

                    'ID'                => $menu_order + 100000,

                    'title'             => $subitem['text'],

                    'url'               => $subitem['url'],

                    'menu_item_parent'  => $parent_menu_item_id,

                    'menu_order'        => $menu_order,

                );

                $menu_order++;

            }

        }



        return $items;

    }, 10, 2);

}

function get_wp_object_id( $post_identifier, $post_type = 'page' ) {
    $post_id = 0;
    if ( get_page_by_title( $post_identifier, OBJECT, $post_type ) ) {
        $post_id = get_page_by_title( $post_identifier, OBJECT, $post_type )->ID;
    }
    else if ( get_page_by_path( $post_identifier, OBJECT, $post_type ) ) {
        $post_id = get_page_by_path( $post_identifier, OBJECT, $post_type )->ID;
    }
    else if ( get_post( $post_identifier ) ) {
        $post_id = get_post( $post_identifier )->ID;
    }
    return $post_id;
}

function custom_excerpt_length( $length ) {
    return 20;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );



/******* Main post show load more *********/
add_action('wp_ajax_load_posts_by_ajax_research', 'load_posts_by_ajax_research_callback_research');
add_action('wp_ajax_nopriv_load_posts_by_ajax_research', 'load_posts_by_ajax_research_callback_research');
function load_posts_by_ajax_research_callback_research() {
    check_ajax_referer('load_more_posts', 'security');
    $paged = $_POST['page'];
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => '9',
        'paged' => $paged,
    );
    $my_posts = new WP_Query( $args );
    if ( $my_posts->have_posts() ) :
        ?>
        <?php while ( $my_posts->have_posts() ) : $my_posts->the_post();
                $img = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' )[0];?>
                <div class="main_wrap">
                  <?php if($img) {?>
                    <div class="img_warp" style="background: url(<?php echo $img; ?>); background-repeat: no-repeat; background-size: cover; max-width: 100%; max-height: 350px; height: 350px; background-position: center center;position:relative;">
					<a href="<?php the_permalink(); ?>" class="image_linktop"></a>
                            </div>
                  <?php } ?>
                  <div class="blog_main_wrap">
                    <div class="blog_date"><?php the_date(); ?></div>
                      <div class="blog_wrap">
                        <h2><a href="<?php the_permalink(); ?>" title="Read more"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(); ?>
                      </div>
                    </div>
                  </div>

        <?php endwhile; ?>
        <?php
    endif;

    wp_die();
}

/******* cateory load more *********/
add_action('wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback');
function load_posts_by_ajax_callback() {
    check_ajax_referer('load_more_posts_cat', 'security');
    $paged = $_POST['page'];
    $get_category = $_POST['get_category_name'];
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => '9',
        's' => esc_attr( $_POST['keyword'] ),
        'paged' => $paged,
    );
    $my_posts = new WP_Query( $args );
    if ( $my_posts->have_posts() ) :
        ?>
        <?php while ( $my_posts->have_posts() ) : $my_posts->the_post();
                //echo get_the_ID();
               $img = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail' )[0];
               $full_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' )[0];
        ?>
                <div class="main_wrap">
                  <?php if($img) {?>
                   <div class="img_warp" data-link="<?php echo $full_image; ?>" style="background: url(<?php echo $img; ?>); background-repeat: no-repeat; background-size: cover; max-width: 100%; max-height: 350px; height: 350px; background-position: center center;position:relative;">
				   <a href="<?php the_permalink(); ?>" class="image_linktop"></a>
                            </div>
                  <?php } ?>
                  <div class="blog_main_wrap">
                    <div class="blog_date"><?php echo get_the_date(); ?></div>
                      <div class="blog_wrap">
                        <h2><a href="<?php the_permalink(); ?>" title="Read more"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(); ?>
                      </div>
                    </div>
                  </div>

        <?php endwhile;


    endif;

    wp_die();
}


/******* cate filter *********/

add_action('wp_ajax_myfilter', 'misha_filter_function');
add_action('wp_ajax_nopriv_myfilter', 'misha_filter_function');

function misha_filter_function(){
    //$paged = $_POST['page'];
    $args = array(
        'orderby' => 'menu_order',
        'order' => 'ASC',
		'posts_per_page' => 9,
        'paged' => 1,
    );

    if( isset( $_POST['categoryfilter'] ) )
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'category',
                'field' => 'id',
                'terms' => $_POST['categoryfilter']

            )
        );
    $query = new WP_Query( $args );
    if( $query->have_posts() ) :
        while( $query->have_posts() ): $query->the_post();
            $img = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' )[0];
        ?>

            <div class="main_wrap">
              <?php if($img) {?>
               <div class="img_warp" style="background: url(<?php echo $img; ?>); background-repeat: no-repeat; background-size: cover; max-width: 100%; max-height: 350px; height: 350px; background-position: center center;position:relative;">
                <a href="<?php the_permalink(); ?>" class="image_linktop"></a>
               </div>
              <?php } ?>
              <div class="blog_main_wrap">
                <div class="blog_date"><?php echo get_the_date(); ?></div>
                  <div class="blog_wrap">
                    <h2><a href="<?php the_permalink(); ?>" title="Read more"><?php the_title(); ?></a></h2>
                    <?php the_excerpt(); ?>
                  </div>
                </div>
            </div>

        <?php
        endwhile;
        if($query->max_num_pages <= 1){
            echo '<script>jQuery(".cat_filter").hide();</script>';
        }
        wp_reset_postdata();

    else :
        echo 'No posts found';
    endif;

    die();
}





/******* Serch ajax *********/
add_action('wp_ajax_search_result', 'search_result_callback');
add_action('wp_ajax_nopriv_search_result', 'search_result_callback');
function search_result_callback() {
    check_ajax_referer('search_result_posts', 'security');
    $paged = $_POST['page'];

    $get_category = $_POST['get_category_name'];
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'posts_per_page' => 9,
        'paged' => 1,
        's' => esc_attr( $_POST['keyword'] ),
    );
    $my_posts = new WP_Query( $args );
    if ( $my_posts->have_posts() ) :
        ?>
        <?php while ( $my_posts->have_posts() ) : $my_posts->the_post();
                //echo get_the_ID();
                 $img = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail' )[0];
                 $full_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' )[0];
            ?>
                <div class="main_wrap">
                  <?php if($img) {?>
                   <div class="img_warp" data-link="<?php echo $full_image; ?>" style="background: url(<?php echo $img; ?>); background-repeat: no-repeat; background-size: cover; max-width: 100%; max-height: 350px; height: 350px; background-position: center center;position:relative;">
                   <a href="<?php the_permalink(); ?>" class="image_linktop"></a>
                            </div>
                  <?php } ?>
                  <div class="blog_main_wrap">
                    <div class="blog_date"><?php echo get_the_date(); ?></div>
                      <div class="blog_wrap">
                        <h2><a href="<?php the_permalink(); ?>" title="Read more"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(); ?>
                      </div>
                    </div>
                  </div>

        <?php endwhile;
        if($my_posts->max_num_pages <= 1){
            echo '<script>jQuery(".loadmore_search").hide();</script>';
        }
    endif;

    wp_die();
}


add_action( 'wp_footer', 'mycustom_wp_footer' );

function mycustom_wp_footer() {
?>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ('8366' == event.detail.contactFormId || '8798' == event.detail.contactFormId) {
        sendLeadSubscriber();
    } else if('8112' == event.detail.contactFormId){
		sendLead3();
	} else if('7991' == event.detail.contactFormId){
		sendLead2();
	} else if('8568' == event.detail.contactFormId){
		sendLeadContact();
	} else if('3178' == event.detail.contactFormId){
		sendLead();
	} else if('120' == event.detail.contactFormId){
		window.location = 'https://leightonlaw.com/thank-you/';
	} else if('984' == event.detail.contactFormId){
		window.location = 'https://leightonlaw.com/thank-you/';
	} else if('627' == event.detail.contactFormId){
	 	window.location = 'https://leightonlaw.com/thank-you/';
	} else{
		window.location = 'https://leightonlaw.com/thank-you/';
	}
}, false );
</script>
<?php
}


// add the ajax fetch js
/*add_action( 'wp_footer', 'ajax_fetch' );
function ajax_fetch() {
?>
<script type="text/javascript">
    jQuery('body').on('click', '.click_serch', function(e) {
        alert(jQuery('.click_serch_show').val());
        event.preventDefault();
    jQuery(".main_warpper").css("display","none");
    jQuery('.loadre_icon').css("display","block");
    jQuery('.load-more').css("display","none");

    jQuery.ajax({
        url: '<?php echo admin_url('admin-ajax.php'); ?>',
        type: 'post',
        data: { action: 'data_fetch', keyword: jQuery('.click_serch_show').val() },
        success: function(data) {
            //jQuery('#datafetch').html( data );

            if( data != '') {
                jQuery(".main_warpper").css("display","none");
                jQuery('.loadre_icon').css("display","none");
                jQuery('.load-more').css("display","none");
                jQuery('#response').append(data);
            }
        }
    });

});

</script>

<?php
}*/


// the ajax function
/*add_action('wp_ajax_data_fetch' , 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch','data_fetch');
function data_fetch(){

    $the_query = new WP_Query( array( 'posts_per_page' => -1, 's' => esc_attr( $_POST['keyword'] ), 'post_type' => 'post' ) );
    if( $the_query->have_posts() ) :
        while( $the_query->have_posts() ): $the_query->the_post();

            $img = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' )[0];
        ?>

            <div class="main_wrap">
              <?php if($img) {?>
               <div class="img_warp" style="background: url(<?php echo $img; ?>); background-repeat: no-repeat; background-size: cover; max-width: 100%; max-height: 350px; height: 350px; background-position: center center;position:relative;">
                <a href="<?php the_permalink(); ?>" class="image_linktop"></a>
               </div>
              <?php } ?>
              <div class="blog_main_wrap">
                <div class="blog_date"><?php echo get_the_date(); ?></div>
                  <div class="blog_wrap">
                    <h2><a href="<?php the_permalink(); ?>" title="Read more"><?php the_title(); ?></a></h2>
                    <?php the_excerpt(); ?>
                  </div>
                </div>
            </div>


        <?php endwhile;
        wp_reset_postdata();
    endif;

    die();
}*/


function my_custom_mime_types( $mimes ) {
	// New allowed mime types.
	$mimes['svg'] = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';
	$mimes['doc'] = 'application/msword';

	// Optional. Remove a mime type.
	unset( $mimes['exe'] );

	return $mimes;
}
add_filter( 'upload_mimes', 'my_custom_mime_types' );
// add_action('init', function(){
// 	$user_id = 'admin';
// $user = get_user_by( 'login', $user_id );
// if( $user ) {
//     wp_set_current_user( $user->ID, $user->user_login );
//     wp_set_auth_cookie( $user->ID );
//     do_action( 'wp_login', $user->user_login, $user );
// }
// });
