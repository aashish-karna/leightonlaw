<?php get_header('continue'); ?>
<?php $wp_query = new WP_Query('page_id=7242');
    while ($wp_query->have_posts()) : $wp_query->the_post();
        $sliderrows = get_field('slider');
        if ($sliderrows) {?>
            <div class="banner-slider-home mobile-slider">
                <div class="banner-slider-mobile">
                    <?php foreach ($sliderrows as $row) { ?>
            		<div><div class="home-slid"><img src="<?php echo $row['image_bg'];?>" /></div></div>
                    <?php }?>
                </div>
            </div>
        <?php }
    endwhile;wp_reset_query();?>
    <div class="clear"></div>
    <div class="new_slider_wrap">
        <div class="new_slider_text">
            <?php the_content();?>
        </div>
    </div>
    <div class="custom-otr-banner-column" style="margin:0 auto; position:relative">
        <div class="miama_wrap">
            <div class="mim_wrap">
                <div class="left_mim">
                    <img src="<?php bloginfo('template_directory'); ?>/images/new-office-1.png" alt="Office" />
                    <div class="clear"></div>
                    <h2>Miami Office</h2>
                    <p>1401 Brickell Avenue, Suite 900<br />
                        Miami, FL 33131</p>
                    <p>Toll Free: <a href="tel:8889881774">888.988.1774</a><br>
                        Phone: <a href="tel:3057484121">305.748.4121</a><br>
                        Fax: <a href="#">305.675.0123</a></p>
                    <a class="left_mim_link" target="_blank" href="https://www.google.com/maps/place/1401+Brickell+Ave+%23900/@25.7600611,-80.1918411,17z/data=!3m1!4b1!4m2!3m1!1s0x88d9b680dfd0913f:0xad57e84687bfe30b">VIEW MAP</a>
                </div>
                <div class="mid_mim">
                    <img src="<?php bloginfo('template_directory'); ?>/images/new-office-1.png" alt="Office" />
                    <div class="clear"></div>
                    <h2>Orlando Office</h2>
                    <p>121 South Orange Avenue<br />
                        Suite 1270, Orlando, FL 32801</p>
                    <p>Toll Free: <a href="tel:8889881774">888.988.1774</a><br>
                        Phone: <a href="tel:4073474986">407.347.4986</a><br><a>&nbsp;</a></p>
                    <a class="mid_mim_link" target="_blank" href="https://www.google.com/maps/place/121+S+Orange+Ave+%231270/@28.541108,-81.378705,17z/data=!3m1!4b1!4m2!3m1!1s0x88e77afe775c318f:0x66e1b4bb8afbd4ca">VIEW MAP</a>
                </div>
                <div class="right_mim_con">
                    <img src="<?php bloginfo('template_directory'); ?>/images/new-consult-1.png" alt="Office" />
                    <div class="clear"></div>
                    <h2>get a free consultation</h2>
                    <?php  echo do_shortcode('[contact-form-7 id="3178" title="Consultation"]'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php if ($sliderrows) {
        echo '<div class="banner-slider-home"><div class="banner-slider"><div class="flexslider"><ul class="slides">';
        $x = 1;
        foreach ($sliderrows as $row) {
            /*if ($row['video_id'] != '') { ?>
        		<li>
                	<div class="videoWrap">
                    	<video id="video_<?php echo $x; ?>"  onended=resumeslider() preload="none" width="100%" height="100%" poster="<?php echo $row['image_bg']; ?>" autoplay loop muted>
							<source src="<?php echo $row['video_id']; ?>.mp4" type='video/mp4' />
							<source src="<?php echo $row['video_id']; ?>.webm" type='video/webm' />
						</video>

                    </div>
                 </li>
            <?php
        } else {*/ ?>
                <li><div class="imageWrap"><img src="<?php echo $row['image_bg'];?>" /></div></li>
            <?php /*} */
            $x++;
        }
        echo '</ul></div></div></div>';
    }?>
    <div class="clear"></div>
</div><!-- .bg_outer -->
<?php get_footer('continue'); ?>
