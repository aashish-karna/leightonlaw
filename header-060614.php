<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/print.css" type="text/css" media="print" />
<link href='//fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'/>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<script language="javascript" type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/jquery-1.8.1.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/logo.js"></script>
<!--<<script language="javascript" type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/jquery-cycle.js"></script>
<script language="javascript" type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/rotation.js"></script>
script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/unlimitedScroller.js"></script>-->
<?php wp_head(); ?>
<script type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>
<body>
<div class="right_contant_new">
<a href="<?php bloginfo('url'); ?>/contact/" class="contt_h" ></a>
<a href="<?php bloginfo('url'); ?>/latest-news/" class="latest_h" ></a>
</div>
<!--........desktop version................-->
<div class="bg_outer_about">
<div class="mid_cont">
<div class="top_head">
<div class="logo"><a href="<?php bloginfo('url') ?>"><img src="<?php bloginfo('template_directory')  ?>/images/logo.png" alt="leighton-law-logo" /></a></div>
<div class="right_head">
<div class="florida_cont">
<h2>Florida personal injury lawyers </h2>
<h3>talk to an attorney  <span>888.395.0001</span></h3>
</div>
<div class="header_nav_container" >
<div class="nav-area">
<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
</div>
</div>
</div>
</div>

<?php include( "php/social-links.php" ); ?>
<div class="clear"></div>
</div>
<div class="clear"></div>
<?php
$cat_id = get_query_var('cat');
$image = get_field('category_header', 'category_'.$cat_id); 
if($image){
?>
<img src="<?php echo $image; ?>" alt="Banner" />
<?php
}elseif(has_post_thumbnail()){
 the_post_thumbnail('full');
}else{ 
?>
<img src="<?php bloginfo('template_directory') ?>/images/about_banner.jpg" alt="Banner" />
<?php } ?>
<div class="clear"></div>
</div>
<!--........desktop version................-->
<script type="text/javascript">
$(document).ready(function(){
	$(".cycle-slideshow").attr("data-cycle-fx","carousel");
	$(".cycle-slideshow").attr("data-cycle-slides","> div")
	$(".cycle-slideshow").attr("data-cycle-carousel-visible","5");
	$(".cycle-slideshow").attr("data-cycle-carousel-fluid","true");
	$(".cycle-slideshow").attr("data-cycle-next",".next");
	$(".cycle-slideshow").attr("data-cycle-prev",".prev");
	$(".cycle-slideshow").attr("data-cycle-timeout","0");
}); 
</script>
<script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/jquery.cycle2.carousel.js"></script>
