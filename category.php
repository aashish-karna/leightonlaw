<?php get_header(); ?>
    <div class="cur_wrap">
        <div class="about_wrap1">
            <div class="mid_cont1">
                <h1 class="pagenewtitle"><?php 
                $cat = get_the_category();
                echo $cat[0]->name;?></h1>
                <div class="content_ab gap_news newPageContent">
                    <div class="left-content">
 <article>
<div>
<?php if ( have_posts() ) : ?>
<div class="cat-list">
    <?php
    query_posts(array( 
        'post_type' => 'post',
        'posts_per_page' => -1,
        'category_name' => $cat[0]->name,
            'orderby'=>'title',
        ) ); 
    while ( have_posts() ) : the_post(); ?>
        <div class="cat-item">
            <?php 
            $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large')?>
            <div class="cat-content">
            <a href="<?php the_permalink();?>"><div class="cat-img" style="background-image: url(<?php echo $image[0];?>)"></div></a>
            <h2><a href="<?php the_permalink();?>" title="Read more"><?php the_title(); ?></a></h2>
            <?php the_excerpt();?>
        </div>
            <div class="btn-list">
                <a class="read-btn" href="<?php the_permalink();?>">Read More</a>
                <a class="help-btn" href="<?php echo site_url();?>/contact">Get Help</a>
            </div>
        </div>
    <?php endwhile; ?>
</div>
    <?php //twentytwelve_content_nav( 'nav-below' ); ?>
    <?php else : ?>
        <?php echo "There is no post related to this category..." ?>
    <?php endif; ?>
<?php //wp_reset_query(); ?>
</div>
</article>
       </div>
        <div class="clear"></div>
                </div>
                <?php include( "php/logo-bar.php" ); ?>
                <div class="sero_cont">
                    <?php echo category_description( get_category_by_slug( 'category' )->term_id ); ?>
                    <?php get_sidebar( 'above' ); ?>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
<?php get_footer(); ?>
