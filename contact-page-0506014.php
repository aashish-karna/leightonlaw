<?php 
/*Template Name: Contact*/
get_header(); ?>

    <div class="cur_wrap">
        <div class="about_wrap1">
            <div class="mid_cont1">
                <div class="content_ab">
                <div class="left-content">
					<div class="cont_cont">
                        <?php if ( have_posts() ) : the_post(); ?>
                            <h2>Contact</h2>
							<h4>*Required fields</h4>
                            <?php the_content(); ?>
                        <?php endif; ?>
						</div>
                    </div>
                    <div class="rightcont_con1_new">
					
<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(__('Sidebar2','lessons-for-us')) ) : else : ?>

<?php endif; ?>

                       <?php /*?> <?php dynamic_sidebar( "sidebar-page" ); ?><?php */?>
                    </div>
                    
                    <div class="clear"></div>
                </div>
                <?php include( "php/logo-bar.php" ); ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>

<?php get_footer(); ?>

