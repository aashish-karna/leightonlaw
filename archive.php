<?php
/* template name: Archives */
 get_header(); ?>

<div id="content">
  <?php if (have_posts()) : ?>
   
  
  <div class="cur_wrap">
    <div class="about_wrap1">
      <div class="mid_cont1">
        <div class="content_ab gap_news newPageContent" >
          <div class="left-content">
            <article>
               <div style="">
				<?php if ( have_posts() ) : ?>
                    <?php
                    while ( have_posts() ) : the_post(); ?>
                        <h2><a href="<?php the_permalink(); ?>" title="Read more"><?php the_title(); ?></a></h2>
                        <?php the_excerpt(); ?>
                    <?php endwhile; ?>
                    <?php twentytwelve_content_nav( 'nav-below' ); ?>
                    <?php else : ?>
                        <?php echo "There is no post related to this category..." ?>
                    <?php endif; ?>
                <?php //wp_reset_query(); ?>
                </div>
              <?php twentytwelve_content_nav( 'nav-below' ); ?>
            </article>
          </div>
        </div>
        <br />
		<br /><br />
		<br /><br />
		 
        <?php include( "php/logo-bar.php" ); ?>
        <?php get_sidebar( 'above' ); ?>
      </div>
    </div>
  </div>
  
  <?php else : ?>
  <div class="post">
    <h2 class="posttitle">
      <?php _e('Not Found') ?>
    </h2>
    <div class="postentry">
      <p>
        <?php _e('Sorry, no posts matched your criteria.'); ?>
      </p>
    </div>
  </div>
  <?php endif; ?>
</div>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
