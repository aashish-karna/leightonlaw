<?php get_header(); ?>
<script type="text/javascript">
  $(document).ready(
      function(){
	      $("#lcp_instance_0 li a").click(
		     function(){
			        $("#lcp_instance_0 li div").slideUp();
			        $("div",$(this).parent()).slideToggle();
			       }
		     );
	     }
      );
</script>
    <div class="cur_wrap">
        <div class="about_wrap1">
            <div class="mid_cont1">
                <div class="content_ab">
                    <div class="left-content" <?php if ( in_category( 30 ) ) : ?>style="width: 655px;"<?php endif; ?>>
					    <?php 
						global $post; 
						if($post->post_name=='news-coverage'){
						   echo '<h1>';
						   the_title(); 
						   echo '</h1>';
						   
						   query_posts('post_type=post&category_name=mda-news-coverage&posts_per_page=-1');
						     if(have_posts()){
							      echo '<ul id="lcp_instance_0" class="lcp_catlist">';
							      while(have_posts()){ the_post();
								       echo '<li>';
									   echo '<a>';
									   the_title();
									   echo '</a>';
									   echo '<div>';
									   the_content();
									   echo '</div>';
									   echo '</li>';
								      }
								  echo '<ul>';
							     }
						   wp_reset_query();
						}else{ 
						?>
                        <?php if ( have_posts() ){ while(have_posts()){ the_post(); ?>
							<h1><?php the_title(); ?></h1>
                            <?php the_content(); ?>
                        <?php }}} ?>
						
						
                    </div>
                    <div class="clear"></div>
                </div>
                <?php include( "php/logo-bar.php" ); ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>

<?php get_footer(); ?>
