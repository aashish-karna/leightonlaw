<div class="footer_ou">
  <div class="mid_cont1">
    <div class="footer">
      <div class="foot_left">
        <div class="foot_left1">
          <div>
            <div style="float:left;"><a href="http://www.resortinjurylawyerblog.com/" target="_blank"><img src="<?php echo home_url();?>/wp-content/uploads/2014/07/resortorts-footer.jpg" alt="" /></a></div>
            <div style="float:left;"><a href="http://www.leightonlaw.co.uk/" target="_blank"><img src="<?php bloginfo('template_url') ?>/images/uk-flag.jpg" alt="flag" title='International Injury Network' /></a></div>
            <!--   <div style="float:left;"><a href="https://leightonlaw.com/latest-news/"><img src="<?php echo home_url();?>/wp-content/uploads/2018/03/Leightons-Latest-E-Newsletter-Masthead-REV-2-18-1800-1.png" alt="flag" title='International Injury Network' /></a></div> -->

          </div>

          <!-- Microdata markup added by Google Structured Data Markup Helper. -->

          <div class="clear"></div>

          <!--  <span itemscope itemtype="http://schema.org/Organization" itemref="_address2">

          <ul>

	<span id="_address2" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" itemref="_addressLocality4 _addressRegion5 _postalCode6">

            <li itemprop="streetAddress">1401 Brickell Avenue, Suite 900</li></span>

            <li><span id="_addressLocality4" itemprop="addressLocality">Miami</span>, <span id="_addressRegion5" itemprop="addressRegion">FL</span> <span id="_postalCode6" itemprop="postalCode">33131</span> <strong>.</strong> <span itemscope itemtype="http://schema.org/LocalBusiness" itemref="_telephone1">

<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" itemref="_addressLocality10 _addressRegion11 _postalCode12">

<span itemprop="streetAddress">121 South Orange Avenue, Suite 1270</span></span></span></li>

            <li> <span id="_addressLocality10" itemprop="addressLocality">Orlando</span>, <span id="_addressRegion11" itemprop="addressRegion">FL</span>  <span id="_postalCode12" itemprop="postalCode">32801</span> </li>

            <li id="_telephone1" itemprop="telephone"> 888.988.1774</li>

	  </span> -->

          </ul>
        </div>
        <div class="foot_left2">
          <p>1401 Brickell Avenue, Suite 900 | Miami, FL 33131 . 121 South Orange Avenue, Suite 1150 | Oriando, FL 32801 | 888.988.1774</p>
        </div>
      </div>
      <div class="footer_right">
        <div class="search-area">
          <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar(__('Search', 'search-bar'))) : else : ?>
          <p>You have not assigned any widget for this section.</p>
          <?php endif; ?>
        </div>

        <!--<img src="images/footer_right.jpg" alt="leighton-law" border="0" />-->

        <a class="cplus" href="http://www.webiotic.com/"  style="float:right;" target="_blank"><img src="<?php bloginfo('template_directory') ?>/images/footer_logo.png" alt="Webiotic" title="Webiotic.com" border="0" /></a><a class="wlogo" href="http://www.webiotic.com/" style="display:none; float:right; margin:1px 0 0 2px" target="_blank"><img src="<?php bloginfo('template_directory') ?>/images/footer-logo_hover.png" alt="web-solutions" title="Webiotic Web Solutions" border="0" /></a> </div>
      <div class="footer-copyright foot_left2">
        <!-- <p>All rights reserved. Leighton Law © 2019.</p> -->
        <?php wp_nav_menu(array( 'container_class' => 'menu-footer', 'theme_location' => 'secondary' )); ?>
      </div>
    </div>
  </div>
</div>

<div id="boxes">
    <div style="top: 25%; left: 50%; display: none;" id="dialog" class="window">
        <div id="san">
            <a href="#" class="close agree">
                <img src="/wp-content/uploads/2009/07/close-icon.png" width="25" style="float:right; margin-right: -25px; margin-top: -20px;"></a>
           <img src="../wp-content/uploads/2009/07/covid-graphic.jpg">
        </div>
    </div>
    <div style="width: 2478px; font-size: 32pt; color:white; height: 1202px; display: none;" id="mask"></div>
</div>
<?php wp_footer(); ?>

<?php

if (is_page(2759)) {
    ?>
	<style type="text/css">
	.g-recaptcha
	{
	    transform: scale(.848) !important;
	    transform-origin: 0 0;
	}
	.cur_wrap	.about_wrap1 .pagenewtitle
	{
		margin-top: 55px;
	}
	.consultation span.wpcf7-not-valid-tip {
	    color: red;
	    font-size: 10px;
	    left: 13px;
	    position: absolute;
	    top: 22px;
	    margin-top: 0!important;
	    font-weight: bold;
	}
	.g-recaptcha iframe #rc-anchor-container .rc-anchor-error-msg-container span.rc-anchor-error-msg{
	   display: none;
	}

	<?php
}
    ?>
</style>
<script type="text/javascript" src="<?php bloginfo("template_directory"); ?>/js/jquery.colorbox.js"></script>
<script>

	jQuery(document).ready(function($){

		jQuery(".group1").colorbox({

			rel:'group1',

			width: '95%',

		    height: '95%',

		    maxWidth: '600px',

		    maxHeight: '550px',

			fixed:true

		});



		$(document).ready(function($) {

          $('.pageNewaccordion .toggle_cat > h2').click(function(e) {

            if($(this).hasClass('down')) {

              $('.pageNewaccordion .lcp_catlist').slideUp();

              $('.pageNewaccordion .toggle_cat > h2').removeClass('down');

            } else {

              $('.pageNewaccordion .lcp_catlist').slideUp();

              $('.pageNewaccordion .toggle_cat > h2').removeClass('down');

              $(this).addClass('down');

              $(this).next('div').slideDown('slow');

            }

          });

        });

	});

  jQuery(document).ready(function($) {



  /* MAIN MENU */

  jQuery('#menu-new-menu  li:has(ul)').addClass('parent');

  jQuery('.parent ul li:has(ul)').addClass('parent');



  jQuery("li.parent").prepend('<span class="plus"></span>');

  //$("li.parent > a").prepend('<span class="arrow"></span>');



  jQuery('.plus').click(function(e) {

    e.preventDefault();

    //jQuery(this).toggleClass('minus');

    //jQuery(this).removeClass("plus").addClass("minus");

    jQuery(this).toggleClass('minus');

    jQuery(this).siblings('ul').slideToggle(300);

    return false;

  });

});

  jQuery(document).ready(function($){

    var url = window.location.href;

    jQuery('span.page-url input').val(url);

  });

  jQuery(document).ready(function($) {

    function deskSlider(){

      jQuery('.nav-menu .verdicts .sub-menu').slick({

        arrows: true,

        dots: false,

        infinite: true,

        cssEase: 'linear',

        speed: 1000,

        slidesToShow: 3,

        slidesToScroll: 1,

        prevArrow: '<button class="slick-prev" aria-label="Previous" type="button"></button>',

        nextArrow: '<button class="slick-next" aria-label="Next" type="button"></button>',

        autoplay: false,

        autoplaySpeed: 1500,

         responsive: [

            {

              breakpoint: 991,

                settings: {

                slidesToShow: 2

                }

            },

            {

              breakpoint: 680,

                settings: {

                slidesToShow: 1

                }

            }

        ]

      });

  }

    deskSlider();

    jQuery('.nav-menu .verdicts .plus').on('click',function(e){

        e.preventDefault();

        jQuery('.nav-menu .verdicts .sub-menu')[0].slick.refresh()

    });

  });



jQuery(document).ready(function($){

var comp = new RegExp(location.host);

$('.single-post .content_ab .p1 a').each(function(){

   if(!comp.test($(this).attr('href'))){

       $(this).attr('target','_blank');

   }

});

});
$.fn.equalHeights = function(){
    var max_height = 0;
    $(this).each(function(){
        max_height = Math.max($(this).height(), max_height);
    });
    $(this).each(function(){
        $(this).height(max_height);
    });
};

$(document).ready(function($){
    $('.eql-height, .equal-text').equalHeights();
});
</script>
<script>






function sendLead() {
	var form = jQuery('form[action*="#wpcf7-f3178-o1"]');
	console.log('submit123 sendLead');
	var pn = form.find('input[name="phone"]').val();

	var x = true;
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  	/*if(pn.trim() == '' || !phoneno.test(pn)) {
		x = false;
	}*/

	if(form.hasClass('invalid')) {
		x = false;
	}
	if(x) {
		jQuery.ajax({
			url: 'https://Leightonlaw.us13.list-manage.com/subscribe/post-json?u=5264cdb0c5b99aa440f3c64d6&amp;id=3e30d98d8f&amp;c=?',
			type: 'GET',
			//async: false,
			dataType: 'json',
			data:{ "NAME" : form.find('input[name="names"]').val(), "PHONE": pn, "EMAIL": form.find('input[name="email"]').val(), "MSG": form.find('input[name="message"]').val() },
			success: function(data){
				console.log(data);
			},
			error: function(data){
				console.log(data);
			}
		});

		jQuery.ajax({
			url: 'https://webiotic-clients.com/leadSystem/FormCall/submitFormData',
			type: 'POST',
			//async: false,
		crossDomain: true,
			dataType: 'html',
			data:{ "name" : form.find('input[name="names"]').val(), "phone": pn, "address": '', "email": form.find('input[name="email"]').val(), "form_name": 'Header inner page form', "comment": form.find('input[name="message"]').val(), "site": "www."+jQuery(document.location).attr('hostname') },
			success: function(data){
         		console.log("q");
				console.log(data);
				window.location = 'https://leightonlaw.com/thank-you/';
			},
			error: function(data){
        		console.log("qQQQQQQQ");
				window.location  = 'https://leightonlaw.com/thank-you/';
				console.log(data);
			}
		});
	}
}














function sendLead2() {
	var form = jQuery('form[action*="#wpcf7-f7991-o2"]');
	console.log('submit123 sendLead2');
	var pn = form.find('input[name="phone-number"]').val();

	var x = true;
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
	/*if(pn.trim() == '' || !phoneno.test(pn)) {
	x = false;
	}*/

	if(form.hasClass('invalid')) {
	x = false;
	}
	if(x) {
	jQuery.ajax({
	  url: 'https://Leightonlaw.us13.list-manage.com/subscribe/post-json?u=5264cdb0c5b99aa440f3c64d6&amp;id=3e30d98d8f&amp;c=?',
	  type: 'GET',
	  //async: false,
	  dataType: 'json',
	  data:{ "NAME" : form.find('input[name="names"]').val(), "PHONE": pn, "EMAIL": form.find('input[name="email"]').val(), "MSG": form.find('input[name="message"]').val() },
	  success: function(data){
	    console.log(data);
	  },
	  error: function(data){
	    console.log(data);
	  }
	});

	jQuery.ajax({
	  url: 'https://webiotic-clients.com/leadSystem/FormCall/submitFormData',
	  type: 'POST',
	  //async: false,
	  crossDomain: true,
	  dataType: 'html',
	  data:{ "name" : form.find('input[name="first-name"]').val(), "phone": pn, "address": '', "email": form.find('input[name="email-70"]').val(), "form_name": 'Footer Form', "comment": form.find('input[name="your-comments"]').val(), "site": "www."+jQuery(document.location).attr('hostname') },
	  success: function(data){
	     console.log("q");
	    console.log(data);
	    window.location = 'https://leightonlaw.com/thank-you/';
	  },
	  error: function(data){
	    console.log("qQQQQQQQ");
	    window.location  = 'https://leightonlaw.com/thank-you/';
	    console.log(data);
	  }
	});
	}
}







function sendLead3() {
	var form = jQuery('form[action*="#wpcf7-f8112-p1866-o2"]');
	console.log('submit123 sendLead3');
	var pn = form.find('input[name="phone"]').val();

	var x = true;
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if(pn.trim() == '' || !phoneno.test(pn)) {
		x = false;
	}
	if(form.hasClass('invalid')) {
		x = false;
	}
	var name = form.find('input[name="client-name"]').val();
	if(x) {
		jQuery.ajax({
			url: 'https://Leightonlaw.us13.list-manage.com/subscribe/post-json?u=5264cdb0c5b99aa440f3c64d6&amp;id=3e30d98d8f&amp;c=?',
			type: 'GET',
			async: false,
			dataType: 'json',
			data:{ "NAME" : name, "PHONE": pn,  "EMAIL": form.find('input[name="email"]').val(), "MSG": form.find('textarea[name="Brieffacts"]').val() },
			success: function(data){
				console.log(data);
			},
			error: function(data){
				console.log(data);
			}
		});

		jQuery.ajax({
			url: 'https://webiotic-clients.com/leadSystem/FormCall/submitFormData',
			type: 'POST',
			async: false,
		crossDomain: true,
			dataType: 'html',
			data:{ "name" : name, "phone": pn, "address": '', "email": form.find('input[name="email"]').val(), "comment": form.find('textarea[name="Brieffacts"]').val(), "site": "www."+jQuery(document.location).attr('hostname') },
			success: function(data){
				console.log(data);
				window.location = 'https://leightonlaw.com/thank-you/';
			},
			error: function(data){
				window.location  = 'https://leightonlaw.com/thank-you/';
				console.log(data);
			}
		});
	}
}






function sendLeadContact() {
	//var form = jQuery('form[action*="#wpcf7-f3179-p3180-o2"]');
	var form = jQuery('form[action*="wpcf7-f8568"]');
	//console.log('submit222 sendLeadContact');
	var pn = form.find('input[name="phone"]').val();

	var x = true;

	if(form.hasClass('invalid')) {
		x = false;
	}
	if(x) {
		jQuery.ajax({
			url: 'https://Leightonlaw.us13.list-manage.com/subscribe/post-json?u=5264cdb0c5b99aa440f3c64d6&amp;id=3e30d98d8f&amp;c=?',
			type: 'GET',
			async: false,
			dataType: 'json',
			data:{ "NAME" : form.find('input[name="names"]').val(), "PHONE": form.find('input[name="phone"]').val(), "address": '', "EMAIL": form.find('input[name="email"]').val(), "MSG": form.find('textarea[name="how"]').val() },
			success: function(data){
				console.log(data);
			},
			error: function(data){
				console.log(data);
			}
		});

		 jQuery.ajax({
			url: 'https://webiotic-clients.com/leadSystem/FormCall/submitFormData',
			type: 'POST',
			async: false,
			crossDomain: true,
			dataType: 'html',
			data:{ "name" : form.find('input[name="names"]').val(), "phone": form.find('input[name="phone"]').val(), "address": form.find('input[name="address"]').val(), "email": form.find('input[name="email"]').val(), "comment": form.find('textarea[name="how"]').val(), "form_name": 'Contact Form', "site": "www."+jQuery(document.location).attr('hostname') },
			success: function(data){
				console.log(data);
				window.location = 'https://leightonlaw.com/thank-you/';
			},
			error: function(data){
				console.log(data);
				window.location = 'https://leightonlaw.com/thank-you/';
			}
		});
	}
}


function sendLeadSubscriber() {
	var form = jQuery('form[action*="#wpcf7-f8366-p8364-o2"]');
	console.log('submit222 sendLeadSubscriber');
	var pn = form.find('input[name="phone"]').val();

	var x = true;

	if(form.hasClass('invalid')) {
		x = false;
	}
	if(x) {
		jQuery.ajax({
			url: 'https://Leightonlaw.us13.list-manage.com/subscribe/post-json?u=5264cdb0c5b99aa440f3c64d6&amp;id=3e30d98d8f&amp;c=?',
			type: 'GET',
			async: false,
			dataType: 'json',
			data:{ "NAME" : form.find('input[name="name-sub"]').val(), "PHONE": form.find('input[name="phone"]').val(), "address": '', "EMAIL": form.find('input[name="email-303"]').val(), "MSG": form.find('textarea[name="how"]').val() },
			success: function(data){
				console.log(data);
			},
			error: function(data){
				console.log(data);
			}
		});

		 jQuery.ajax({
			url: 'https://webiotic-clients.com/leadSystem/FormCall/submitFormData',
			type: 'POST',
			async: false,
			crossDomain: true,
			dataType: 'html',
			data:{ "name" : form.find('input[name="name-sub"]').val(), "phone": '123456789', "address": "", "email": form.find('input[name="email-303"]').val(), "comment": " ", "form_name": 'Subscriber', "site": "www."+jQuery(document.location).attr('hostname') },
			success: function(data){
				console.log(data);
				window.location = 'https://leightonlaw.com/thank-you/';
			},
			error: function(data){
				console.log(data);
				window.location = 'https://leightonlaw.com/thank-you/';
			}
		});
	}
}

/*** Blog Page Search and loadmore ***/

jQuery(function($) {
    var page = 2;
    jQuery('body').on('click', '.loadmore_search', function(e) {
        event.preventDefault();
        var serch_show = jQuery('.click_serch_show').val();
        var ajaxurl1 = "<?php echo admin_url('admin-ajax.php'); ?>";
        var button = $(this).html();
        $(".loadmore_search").html('<p>Loading..</p>');

        var data = {
            'action': 'load_posts_by_ajax',
            'page': page,
            'keyword' : serch_show,
            'security': '<?php echo wp_create_nonce("load_more_posts_cat"); ?>'
        };

        $.post(ajaxurl1, data, function(response) {
            //console.log(response);
            if( response != '') {
                $(".loadmore_search").html('<p>Load more</p>');
                jQuery('#response').append(response);
                jQuery('.img_warp').each(function(){
    				jQuery(this).css('background-image','url('+jQuery(this).attr('data-link'));
  				});
                page++;
            } else {
                $(".loadmore_search").html('<p>Load more</p>');
                jQuery('.cat_filter.loadmore_search').css("display","none");
            }
        });
    });
});

jQuery(function($) {
    jQuery('#filter_search').submit(function(e) {
        event.preventDefault();
        jQuery('.loadre_icon').html("<img src='https://loading.io/spinners/wave/index.wave-ball-preloader.svg'>");
        jQuery(".main_warpper").css("display","none");
        jQuery('.loadre_icon').css("display","block");
        jQuery('.load-more').css("display","none");
        jQuery('.cat_filter.loadmore_search').css("display","none");
        jQuery('#response').css("display","none");
        jQuery('.blog-navigation').css("display","none");
        var ajaxurl1 = "<?php echo admin_url('admin-ajax.php'); ?>";
        var serch_show = jQuery('.click_serch_show').val();
        var button = $(this).html();
        $(".loadmore").html('<p>Loading..</p>');
        var data = {
            'action': 'search_result',
            'keyword': serch_show,
            'security': '<?php echo wp_create_nonce("search_result_posts"); ?>'
        };

        $.post(ajaxurl1, data, function(response) {
            //console.log(response);
            if( response != '') {
                jQuery(".main_warpper").css("display","none");
                jQuery('.loadre_icon').css("display","none");
                jQuery('.load-more').css("display","none");
                jQuery('#response').css("display","block");
                jQuery('.cat_filter.loadmore_search').css("display","block");
                jQuery('#response').html(response);
                jQuery('.img_warp').each(function(){
                    jQuery(this).css('background-image','url('+jQuery(this).attr('data-link'));
  				});
            } else {
               jQuery('.loadre_icon').html("No Post Found");
            }
        });
    });
});

</script>
<script type="text/javascript">
jQuery(window).load(function() {
  jQuery('.img_warp').each(function(){
    jQuery(this).css('background-image','url('+jQuery(this).attr('data-link'));
  });


});
</script>

<script type="text/javascript">
	jQuery(document).ready(function($) {
        $(document).on('click', '.covid-19-message a', function(e){
    		var id = '#dialog';

    		//Get the screen height and width
    		var maskHeight = $(document).height();
    		var maskWidth = $(window).width();

    		//Set heigth and width to mask to fill up the whole screen
    		$('#mask').css({'width':maskWidth,'height':maskHeight});

    		//transition effect
    		$('#mask').fadeIn(500);
    		$('#mask').fadeTo("slow",0.9);

    		//Get the window height and width
    		var winH = $(window).height();
    		var winW = $(window).width();

    		//Set the popup window to center
    		$(id).css('top',  winH/2-$(id).height()/2);
    		$(id).css('left', winW/2-$(id).width()/2);

    		//transition effect
    		$(id).fadeIn(2000);
        });

	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();

		$('#mask').hide();
		$('.window').hide();
	});

	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});

});



</script>

</body></html>
