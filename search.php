<?php get_header(); ?>

<div id="content">
  <div class="cur_wrap">
    <div class="about_wrap1">
      <div class="mid_cont1">
        <div class="content_ab" style="min-height:700px;margin-bottom:40px;">
          <?php if (have_posts()) : ?>
         
          <?php while (have_posts()) : the_post(); ?>
          <div class="post" id="post-<?php the_ID(); ?>">
            <h3 class="posttitle" style='margin-top:30px;margin-bottom:30px;'><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>">
              <?php the_title(); ?>
              </a></h3>
            <div class="postentry">
              <?php the_excerpt(); ?>
            </div>
            <div class="postmetadata" style='margin-left:17px;font-size:12px;'>
              <a href="<?php the_permalink() ?>">Read More</a>
            </div>
          </div>
          <?php endwhile; ?>
          <?php else : ?>
          <div class="post">
            <h2 class="posttitle">
              <?php _e('Not Found') ?>
            </h2>
            <div class="postentry">
              <p>
                <?php _e('Sorry, no posts matched your criteria.'); ?>
              </p>
            </div>
          </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>