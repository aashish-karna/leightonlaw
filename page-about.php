<?php 
/*Template Name: New Page template */
get_header(); 
?>
 <?php while ( have_posts() ) : the_post(); ?>
    <div class="cur_wrap">
        <div class="about_wrap1">
        	
            <div class="mid_cont1">
            <h1 class="pagenewtitle"><span><?php echo get_the_title(); ?></span> <span class="starsImg"><img src="<?php bloginfo('template_directory') ?>/images/star.png" alt="star" /><img src="<?php bloginfo('template_directory') ?>/images/star.png" alt="star" /><img src="<?php bloginfo('template_directory') ?>/images/star.png" alt="star" /><img src="<?php bloginfo('template_directory') ?>/images/star.png" alt="star" /><img src="<?php bloginfo('template_directory') ?>/images/star.png" alt="star" /></span></h1>
                <div class="content_ab gap_news newPageContent">
                		<?php //dynamic_sidebar( "sidebar-page" ); ?>
					  
                       	<?php the_content(); ?>
                      
                    <div class="clear"></div>
						<div class="pageNewaccordion">
							<?php 
                            $rows = get_field('accordion');
                            if($rows)
                            {
                                foreach($rows as $row)
                                { ?>
                                	<div class="toggle_cat">
                                        <h2 class="up"><?php echo $row['accordion_title']; ?></h2>
                                        <div class="lcp_catlist">
                                            <?php echo $row['accordion_content']; ?>
                                        </div>
                                    </div>
                               <?php }
                            }
                            ?>
                            <div class="toggle_cat">
                            	<?php 
                                if (types_render_field( "gallery-image-side")) { ?>
                                <h2 class="up">Gallery</h2>
                                <div class="lcp_catlist">
                                    <div class="sidebar-gallery">
                                        <ul>
                                          <?php 
                                          $slider = types_render_field( "gallery-image-side", array( "width" => "300", "height" => "200", "proportional" => "true", "output"=>"raw", "separator"=>", " ) );
                                          $myArray = explode(', ', $slider);
                                          foreach($myArray as $my_Array){
                                           echo '<li><a href="'.$my_Array.'" class="group1 cboxElement"><img src="'.$my_Array.'" /></a></li>';  $attch_id = get_attachment_id_from_src($my_Array);
                                      $attachment = get_post( $attch_id );
                                        //echo get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true );
                                        if($attachment->post_excerpt){ echo '<p>'. $attachment->post_excerpt.'</p>'; }
                                        //echo $attachment->post_content;
                                        //echo get_permalink( $attachment->ID );
                                        //echo $attachment->guid;
                                        //echo '<p>'.$attachment->post_title.'<p>';
                                          }
                                          ?>
                                        </ul>
                                        
                                    </div>
                                </div>
                                <style>
                                .right_mim_con1 {
                                    padding-bottom: 20px;
                                }
                                </style>
                               <?php } ?>
                            </div> 
                            
                        </div>
                    <div class="clear"></div>
                </div>
                <?php include( "php/logo-bar.php" ); ?>
				<?php get_sidebar( 'above' ); ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
 <?php endwhile; ?>
<?php get_footer(); ?>
