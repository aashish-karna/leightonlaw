<?php get_header(); ?>

    <div class="cur_wrap">
        <div class="about_wrap1">
            <div class="mid_cont1">
                <div class="content_ab">
                    <div class="right-sidebar">
                        <?php dynamic_sidebar( "sidebar-page" ); ?>
                    </div>
                    <div class="left-content">
                        <?php if ( have_posts() ) : the_post(); ?>
                            <h1><?php the_title(); ?></h1>
                            <?php the_content(); ?>
                        <?php endif; ?>
                    </div>
                    <div class="clear"></div>
                </div>
                <?php include( "php/logo-bar.php" ); ?>
				<div class="wrap_watch" style="margin-top: 45px;">
					<div class="iner_wat gap1">
						<p><img src="<?php bloginfo( "template_directory" ); ?>/images/video_img1.jpg" alt="Video" /></p>
						<p>John Leighton appears on CNN to comment on proposed parasailing safety regulations.</p>
						<p style="min-height:0px"><a href="#">watch video</a></p>
					</div>
					<div class="iner_wat gap2">
						<p><img src="<?php bloginfo( "template_directory" ); ?>/images/video_img2.jpg" alt="Video" /></p>
						<p>John Leighton appears on NBC's "Today" show as part of his representation of the family of Amber May White, who was killed in a parasailing tragedy.</p>
						<p style="min-height:0px"><a href="">watch video</a></p>
					</div>
					<div class="iner_wat gap3">
						<p><img src="<?php bloginfo( "template_directory" ); ?>/images/video_img3.jpg" alt="Video" /></p>
						<p>John Leighton is featured on the cover of Attorney At Law magazine as its "Attorney of the Month".</p>
						<p style="min-height:0px"><a href="#">read article</a></p>
					</div>
					<div class="iner_wat2 gap4">
						<p><img src="<?php bloginfo( "template_directory" ); ?>/images/video_img4.jpg" alt="Video" /></p>
						<p>John Leighton's 2 volume book Litigating  Premises Security Cases.</p>
						<p style="min-height:0px"><a href="#">order now</a></p>
					</div>
				</div>
            </div>
        </div>
        <div class="clear"></div>
    </div>

<?php get_footer(); ?>
