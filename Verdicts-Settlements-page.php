<?php 
/*Template Name: Category accordion */
get_header(); 
?>
<script type="text/javascript">
  $(document).ready(function(){
      
	  $(".toggle_cat h2").click(function()
	  {
			var openMe = $(this).next();
			//alert(openMe)
			var mySiblings = $(this).parent().siblings().find('ul');
			
			if (openMe.is(':visible')) {
				
				openMe.slideUp();
				$(this).parent().find("ul").slideUp();
				$(this).removeClass('down');
				
			} else {
				//
				mySiblings.slideUp();  
				openMe.slideDown();
				$(".toggle_cat h2").removeClass('down');
				$(this).addClass('down');
				
			}
	      
	  });
		
	     });
      
</script>
    <div class="cur_wrap">
        <div class="about_wrap1">
            <div class="mid_cont1">
                <div class="content_ab gap_news">
					   <!-- <h1>News</h1>-->
						
					    <?php 
						$news_cat = array();
						while(have_posts()){ the_post();
							$rows = get_field('select_category');
						 
							if($rows) {
								foreach($rows as $row) {
									$c = $row['category_for_accordion'];
									array_push($news_cat,$c->slug);
								}
							}
						}
						    
						   foreach($news_cat as $cat){
						   $getcat = get_category_by_slug($cat);
						   echo '<div class="toggle_cat">';
						   echo '<h2 class="up">'.str_replace('cr ', '', $getcat->name).'</h2>';
						   query_posts('post_type=post&category_name='.$cat.'&posts_per_page=-1&orderby=date&order=DESC');
						     if(have_posts()){
							      echo '<ul id="lcp_instance_0" class="lcp_catlist">';
						?>
						
						<?php
						  while(have_posts()){ the_post();
							echo '<li>';
							echo '<a href="'.get_permalink().'">';
							the_title();
							echo '</a>';
							echo '</li>';
						   }
						?>
						
						<?php
								  echo '<ul>';
							     }
						   wp_reset_query();
						   echo '</div>';
						}
						?>
						
                    <div class="clear"></div>
                </div>
                <?php include( "php/logo-bar.php" ); ?>
				<?php get_sidebar( 'above' ); ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>

<?php get_footer(); ?>