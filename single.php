<?php get_header(); ?>

<script type="text/javascript">

  $(document).ready(

      function(){

	      $("#lcp_instance_0 li a").click(

		     function(){

			        $("#lcp_instance_0 li div").slideUp();

			        $("div",$(this).parent()).slideToggle();

			       }

		     );

	     }

      );

</script>
<style type="text/css">
.single-post h1.pagenewtitles {
    margin-top: 0;
    font-family: Merriweather;
    font-family: Oswald;
    color: #08619e;
    font-weight: 700;
    font-size: 35px;
}
.single-post .content_ab p {
    color: #000;
}
.single-post h2 {
    color: #232323 !important;
    font-family: Oswald !important;
}    
.about_wrap1 {
  background: none;
}
.content_ab.gap_news.newPageContent {
    margin-top: -250px;
}
.single-post .footer {
  background: none;
}
.newPageContent {
  color: #232323 !important;
}
.newTemplatePageForm {
  display: none;
}
.bg_outer_about::before {
    display: table;
}
.buttons_blog {
    text-align: center;
}
.previous-button a {
    background: #979797;
    color: #FFFF;
    font-weight: 600;
   padding: 7px 45px;
}
.next-button a {
    background: #ff670e;
    color: #FFFF;
    font-weight: 600;
    padding: 7px 45px;
}
.buttons_blog {
    text-align: center;
    margin-top: 90px;
    margin-bottom: 50px;
}
span.next-button {
    margin-left: 50px;
}
.get-started {
    display: block;
    clear: both;
    background-position: center center;
    background-repeat: no-repeat;
    padding: 80px 0 55px;
    background-size: cover;
    width: 100%;
}
.get-started .mid_cont1 {
    width: 1005px;
    margin: 0 auto;
    position: relative;
}
.get-started h2 {
    color: #ffffff !important;
    font-family: "Open Sans" !important;
    font-size: 50px;
    font-weight: 700;
    text-transform: uppercase;
    text-align: center;
    margin-bottom: 50px;
}
.get-started .form-col {
    margin-bottom: 15px;
    max-width: 33.33%;
    flex: 0 0 33.33%;
    padding: 0 15px;
    box-sizing: border-box;
}
.get-started .form-col.col-textarea {
    max-width: 99.7%;
    flex: 0 0 99.7%;
}
.get-started .form-submit-footer-btn.form-col-full {
    margin: 15px auto 0;
}
.get-started .started-form {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    margin: 0 -15px;
}
.get-started .form-col .first-name:before, .form-col .last-name:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/first_name.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 12px;
    height: 14px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}
.get-started .form-col .email-70:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/email_box.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 19px;
    height: 15px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}
.get-started .form-col .phone-number:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/phone_number.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 11px;
    height: 15px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}
.get-started .form-col .type-service:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/type_service.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 22px;
    height: 21px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}
.get-started .form-col .about-us:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/about_us.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 10px;
    height: 17px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}
.get-started .form-col .your-comments:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/comment.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 16px;
    height: 22px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}

.get-started .form-col input, .form-col textarea, .form-col select {
    background: transparent;
    box-shadow: none;
    border-left: none;
    border-radius: 0;
    border-right: none;
    border-top: none;
    border-bottom: 1px solid #fff;
    padding-bottom: 33px;
    color: #ffffff;
    font-family: "Open Sans";
    font-size: 14px;
    font-weight: 400;
    max-width: 100%;
    text-indent: 30px;
    line-height: 20px;
    width: 100%;
    outline: none !implode;    
}
.form-col select {
    background-position: right 0px top 10px;
    background-image: url(https://leightonlaw.com/wp-content/themes/leighton/images/down-arrow-select.png);
    -webkit-appearance: none;
    background-repeat: no-repeat;
}
.get-started .form-col.col-textarea .wpcf7-form-control-wrap {
    display: block;
}
.get-started .form-submit-footer-btn input.wpcf7-submit {
    line-height: 24px !important;
    height: inherit !important;
    background: #FE680D !important;
    padding: 13px 0 16px!important;
    transition: 0.3s all;
    -webkit-transition: 0.3s all;
    text-decoration: none;
    border: none;
    color: #ffffff;
    font-family: Oswald;
    font-size: 24px;
    font-weight: 500;
    width: 214px;
    cursor: pointer;
}
.get-started .wpcf7-form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #FFF;
}
.get-started .wpcf7-form-control::-moz-placeholder { /* Firefox 19+ */
  color: #FFF;
}
.get-started .wpcf7-form-control:-ms-input-placeholder { /* IE 10+ */
  color: #FFF;
}
.get-started .wpcf7-form-control:-moz-placeholder { /* Firefox 18- */
  color: #FFF;
}
.artist_blog{
   
}
.artist_blog .flexbox{
    display: flex;
    flex-wrap: wrap;
     background-color: #fff;
}
.artist_blog  .col{
    flex: 0 0 50%;
    max-width: 50%;
    box-sizing: border-box;
    padding: 0 15px;
    color: #515151;
}
.artist_blog  .col span,
.artist_blog  .col h4{
    font-family: Roboto,sans-serif;
}
.artist_blog  .col.card-left{
       display: flex;
    align-items: flex-end;
}
.artist_blog  .col h4{
    font-size: 36px;
    line-height: 36px;
    color: #515151;
    padding: 15px 0 20px;
}
.artist_blog  .col p{
    font-size: 18px;
   line-height: 28px;

    color: #515151;
}
.artist_blog  .col .card-wrap{
   padding: 18px 10px;
}
.social_article{
    padding-top: 25px;
}
.social_article ul{
    display: flex;
    align-items: center;
    padding: 0;
    list-style: none;
    margin: 0;
}
.social_article ul li{
    padding: 0 5px;
}
.social_article ul li a{
    display: inline-block;
    transition: all 0.2s;
}
.social_article a:hover{
   opacity: 0.5;
}
.suggested_section {
    clear: both;
    overflow: hidden;
    padding: 70px 0px;
}
.suggested_section .mid_cont1 h2.sec_title_sr {
    font-size: 35px;
    color: #08619e !important;
    font-weight: 700;
    padding: 0px 0px 50px 0px;
    text-transform: uppercase;
}
.sr_warp {
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    justify-content: space-between;
}
.sr_inner {
    width: 48%;
    position: relative;
}
.sr_inner h4 {
    position: absolute;
    left: 0;
    bottom: 0;
    background: rgba(255,255,255,0.8);
    color: #08619e;
    padding: 20px;
    font-family: Oswald;
    font-size: 20px;
    max-width: 100%;
    width: 100%;
    box-sizing: border-box;
    text-transform: uppercase;
    font-weight: 500;
}
.sr_inner img {
    height: 280px;
    object-fit: cover;
    width: 100%;
    display: block;
}
.sr_inner {
    transition: 0.5s all; 
}
.sr_inner:hover {
    box-shadow: 0px 0px 15px 5px #005a98;
}
@media screen and (max-width: 950px) {
    .sr_inner {
        width: 100%;
        margin-bottom: 20px;
        position: relative;
    }   
}
@media only screen and (max-width: 850px) {
    .artist_blog .col{
        max-width: 100%;
        flex:0 0 100%;
    }
    .artist_blog .flexbox{
        max-width: 470px;
            margin: 0 auto;
    }
    .artist_blog .col span.article-lebal{
        font-size: 14px;
    }
    .artist_blog .col h4{
            font-size: 28px;
        line-height: 28px;
        padding: 8px 0 11px;
    }
    .card-img{
        max-width: 80%;
    margin: 0 auto;
    padding: 25px 25px 0;
    }
    .get-started .started-form{
        margin: 0; 
    }
}
@media only screen and (max-width: 767px) {
    .get-started .form-col{
            max-width: 100%;
            flex: 0 0 100%;
    }
}
@media only screen and (max-width: 540px) {
     .artist_blog .col p{
        font-size: 14px;
        line-height: 22px;
     }
     .social_article{
        padding-top: 10px;
     }
     .card-img{
            max-width: 90%;
     }
}
</style>


<?php $current_post = get_the_ID(); ?>

    <div class="cur_wrap">

        <div class="about_wrap1">

            <div class="mid_cont1">

                <div class="content_ab gap_news newPageContent">

				

				<?php $slug = $post->post_name; ?>			 

				    <h1 class="pagenewtitles"><?php echo get_the_title(); ?></h1>


				<?php if( $slug =='john-e-leighton-named-to-the-best-lawyers-in-america-for-7th-consecutive-year' || $slug =='your-cruise-vacation-may-be-much-more-adventurous-than-you-anticipated-major-cruise-lines-admit-faults-with-cruise-safety' || $slug == 'john-e-leighton-featured-as-expert-on-parasailing-safety-in-south-florida-legal-guide'){	?>

				    <div class="post-cont">				

				<?php } else { ?>

				<div class="left-content" <?php if ( in_category( 30 ) ) : ?>style="width: 655px;"<?php endif; ?>>

				<?php } ?>

			 	 				

                    <?php /*?><div class="left-content" <?php if ( in_category( 30 ) ) : ?>style="width: 655px;"<?php endif; ?>><?php */?>

					    <?php 

						global $post; 

						if($post->post_name=='news-coverage'){

						   echo '<h1>';

						   the_title(); 

						   echo '</h1>';

						   

						   query_posts('post_type=post&category_name=mda-news-coverage&posts_per_page=-1');

						     if(have_posts()){

							      echo '<ul id="lcp_instance_0" class="lcp_catlist">';

							      while(have_posts()){ the_post();

								       echo '<li>';

									   echo '<a>';

									   the_title();

									   echo '</a>';

									   echo '<div>';

									   the_content();

									   echo '</div>';

									   echo '</li>';

								      }

								  echo '<ul>';

							     }

						   wp_reset_query();

						}else{ 

						?>

                        <?php if ( have_posts() ){ while(have_posts()){ the_post(); ?>

							<?php the_content(); ?>

                            <div class="clear"></div>

                                <div class="pageNewaccordion">

                                    <?php 

                                    $rows = get_field('accordion');

                                    if($rows)

                                    {

                                        foreach($rows as $row)

                                        { ?>

                                            <div class="toggle_cat">

                                                <h2 class="up"><?php echo $row['accordion_title']; ?></h2>

                                                <div class="lcp_catlist">

                                                    <?php echo $row['accordion_content']; ?>

                                                </div>

                                            </div>

                                       <?php }

                                    }

                                    ?>

                                    <div class="toggle_cat">

                                        <?php 

                                        if (types_render_field( "gallery-image-side")) { ?>

                                        <h2 class="up">Gallery</h2>

                                        <div class="lcp_catlist">

                                            <div class="sidebar-gallery">

                                                <ul>

                                                  <?php 

                                                  $slider = types_render_field( "gallery-image-side", array( "width" => "300", "height" => "200", "proportional" => "true", "output"=>"raw", "separator"=>", " ) );

                                                  $myArray = explode(', ', $slider);

                                                  foreach($myArray as $my_Array){

                                                   echo '<li><a href="'.$my_Array.'" class="group1 cboxElement"><img src="'.$my_Array.'" /></a></li>';  $attch_id = get_attachment_id_from_src($my_Array);

                                              $attachment = get_post( $attch_id );

                                                //echo get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true );

                                                if($attachment->post_excerpt){ echo '<p>'. $attachment->post_excerpt.'</p>'; }

                                                //echo $attachment->post_content;

                                                //echo get_permalink( $attachment->ID );

                                                //echo $attachment->guid;

                                                //echo '<p>'.$attachment->post_title.'<p>';

                                                  }

                                                  ?>

                                                </ul>

                                                

                                            </div>

                                        </div>

                                        <style>

                                        .right_mim_con1 {

                                            padding-bottom: 20px;

                                        }

                                        </style>

                                       <?php } ?>

                                    </div> 

                                </div>

                               <!--  <div class="buttons_blog">
                                    <span class="previous-button"><?php //next_post_link( '%link', 'BACK', TRUE ) ?></span>
                                    <span class="next-button "><?php //previous_post_link( '%link', 'NEXT ARTICLE', TRUE ) ?></span>
                                </div> -->

                            <div class="clear"></div>

                        <?php }}} ?>


                    </div>
                    <?php $post_categories = wp_get_post_categories( $current_post );
                    $catname = array();
                             
                    foreach($post_categories as $c){
                        $cat = get_category( $c );
                        $catname[] = $cat->slug;
                    } 
                    if($catname[0] == 'blog-post' || $catname[1] == 'blog-post' || $catname[2] == 'blog-post' || $catname[3] == 'blog-post') {
                    ?>

                        <div class="artist_blog">
                            <div class="flexbox">
                                <div class="col card-left">
                                    <div class="card-img">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/article.png" alt=""/>
                                    </div>
                                </div>
                                <div class="col card-right">
                                    <div class="card-wrap">
                                        <span class="article-lebal">Article by:</span>
                                        <h4>John Leighton</h4>
                                        <p>A nationally-recognized trial lawyer who handles catastrophic injury and death cases.  He manages Leighton Law, P.A. trial lawyers, with offices in Miami and Orlando, Florida. He is President of The National Crime Victim Bar Association, author of the 2-volume textbook,Litigating Premises Security Cases, and past Chairman of the Association of Trial Lawyers of America’s Motor Vehicle, Highway & Premises Liability Section. Having won some of the largest verdicts in Florida history, Mr. Leighton is listed inThe Best Lawyers in America (14  years), “Top Lawyers” in the South Florida Legal Guide (15 years), Top 100 Florida SuperLawyer™ and Florida SuperLawyers (14 years), “Orlando Legal Elite” by Orlando Style magazine, and FloridaTrend magazine “Florida Legal Elite</p>

                                        <div class="social_article">
                                            <ul>
                                                <li><a href="https://www.facebook.com/LeightonLaw" target="_blank"><svg height="40px" id="Layer_1" version="1.1" viewBox="0 0 512 512" width="40px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:svg="http://www.w3.org/2000/svg"><defs id="defs12"/><g id="g5991"><rect height="512" id="rect2987" style="fill:#b1b1b1;fill-opacity:1;fill-rule:nonzero;stroke:none" width="512" x="0" y="0"/><path d="M 286.96783,455.99972 V 273.53753 h 61.244 l 9.1699,-71.10266 h -70.41246 v -45.39493 c 0,-20.58828 5.72066,-34.61942 35.23496,-34.61942 l 37.6554,-0.0112 V 58.807915 c -6.5097,-0.87381 -28.8571,-2.80794 -54.8675,-2.80794 -54.28803,0 -91.44995,33.14585 -91.44995,93.998125 v 52.43708 h -61.40181 v 71.10266 h 61.40039 v 182.46219 h 73.42707 z" id="f_1_" style="fill:#ffffff"/></g></svg></a></li>
                                                 <li><a href="https://www.linkedin.com/in/leightonlaw" target="_blank"><svg height="45px" id="Layer_1" style="enable-background:new 0 0 67 67;" version="1.1" viewBox="0 0 67 67" width="45px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M49.837,48.137V36.425c0-6.275-3.35-9.195-7.816-9.195  c-3.604,0-5.219,1.983-6.119,3.374V27.71h-6.79c0.09,1.917,0,20.427,0,20.427h6.79V36.729c0-0.609,0.044-1.219,0.224-1.655  c0.49-1.22,1.607-2.483,3.482-2.483c2.458,0,3.44,1.873,3.44,4.618v10.929H49.837z M21.959,24.922c2.367,0,3.842-1.57,3.842-3.531  c-0.044-2.003-1.475-3.528-3.797-3.528s-3.841,1.524-3.841,3.528c0,1.961,1.474,3.531,3.753,3.531H21.959z M25.354,48.137V27.71  h-6.789v20.427H25.354z M3,4h60v60H3V4z" style="fill-rule:evenodd;clip-rule:evenodd;fill:#b1b1b1;"/></svg></a></li>
                                                  <li><a href="https://twitter.com/leightonlawfla" target="_blank"><svg height="45px" id="Layer_1" style="enable-background:new 0 0 67 67;" version="1.1" viewBox="0 0 67 67" width="45px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M37.167,22.283c-2.619,0.953-4.274,3.411-4.086,6.101  l0.063,1.038l-1.048-0.127c-3.813-0.487-7.145-2.139-9.974-4.915l-1.383-1.377l-0.356,1.017c-0.754,2.268-0.272,4.661,1.299,6.271  c0.838,0.89,0.649,1.017-0.796,0.487c-0.503-0.169-0.943-0.296-0.985-0.233c-0.146,0.149,0.356,2.076,0.754,2.839  c0.545,1.06,1.655,2.098,2.871,2.712l1.027,0.487l-1.215,0.021c-1.173,0-1.215,0.021-1.089,0.466  c0.419,1.377,2.074,2.839,3.918,3.475l1.299,0.444l-1.131,0.678c-1.676,0.975-3.646,1.525-5.616,1.568  C19.775,43.256,19,43.341,19,43.404c0,0.212,2.557,1.398,4.044,1.864c4.463,1.377,9.765,0.784,13.746-1.567  c2.829-1.674,5.657-5,6.978-8.22c0.713-1.716,1.425-4.852,1.425-6.355c0-0.975,0.063-1.102,1.236-2.267  c0.692-0.679,1.341-1.419,1.467-1.631c0.21-0.403,0.188-0.403-0.88-0.044c-1.781,0.637-2.033,0.552-1.152-0.401  c0.649-0.678,1.425-1.907,1.425-2.268c0-0.062-0.314,0.042-0.671,0.233c-0.377,0.212-1.215,0.53-1.844,0.72l-1.131,0.361l-1.027-0.7  c-0.566-0.381-1.361-0.805-1.781-0.932C39.766,21.902,38.131,21.944,37.167,22.283z M3,4h60v60H3V4z" style="fill-rule:evenodd;clip-rule:evenodd;fill:#b1b1b1;"/></svg></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="clear"></div>

                </div>

               <!--  <?php include( "php/logo-bar.php" ); ?>

				<?php get_sidebar( 'above' ); ?> -->

            </div>

        </div>

        <div class="clear"></div>

    </div>

<div class="suggested_section">
    <div class="mid_cont1">
        <h2 class="sec_title_sr">Suggested Read</h2>
        <div class="sr_warp">
            <?php 
                $args = array(
                    'posts_per_page' => '2',
                    'post_type' => 'post',
                    'orderby' => 'post_date',
                    'post__not_in' => array($current_post),
                    'order'   => 'DESC',
                    'post_status' => 'publish',
                );
                $my_posts = new WP_Query( $args );
                
                if ( $my_posts->have_posts() ) :

            ?>
            <?php while ( $my_posts->have_posts() ) : $my_posts->the_post();
                $img = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_id()), 'full' )[0]; ?>
               
                    <div class="sr_inner">
                        <a href="<?php the_permalink(); ?>">
                            <div class="sr_inner_img">
                                <?php if($img) { ?>
                                    <img src="<?php echo $img ?>" alt="Suggested Read"/>
                                <?php } else { ?>
                                    <img src="<?php echo site_url(); ?>/wp-content/uploads/2019/07/blogbanner.jpg" alt="Suggested Read"/>
                                <?php } ?>    
                            </div>
                            <h4><?php the_title(); ?></h4>
                        </a>
                    </div>
                

            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>    
</div>  


<div class="get-started" style="background-image: url('<?php echo get_field('background_image', 7242);?>');">
  <div class="mid_cont1">
    <h2><?php echo get_field('form_title', 7242);?></h2>
    <?php echo do_shortcode(get_field('contact_form_shortcode', 7242));?>
  </div>
</div>

<?php get_footer(); ?>

