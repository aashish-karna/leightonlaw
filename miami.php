<?php /* Template Name: miami-page */ ?>
<?php get_header(); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/new-home.css?ver=0.5">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style type="text/css">
	
	.images_text p {
	    padding-left: 20px;
	}	
	.images_text {
	    display: flex;
	}
	.privacy_content h1 {
	    margin-top: 0;
	    font-family: Merriweather;
	    font-family: Oswald;
	    color: #08619e;
	    font-weight: 700;
	    font-size: 35px;
	}
	
	.privacy_content li{
		text-transform: initial;
	}
	.title_content {
	    text-transform: initial;
	}

		.content_ab.gap_news.newPageContent {
    		display: flex;
    		justify-content: space-between;
    		margin-top: -434px !important;
		}
		.right-content {
		    width: 100%;
		    max-width: 650px;
		    /*padding:0px 20px 0px 10px;*/
		}
		.right_mim_con1{
			display: none;
		}
		.catgory_post_title
		{
			color: #2a5b9a;
			font-weight: 600;
		}
		.post_title
		{
			color: black;
		}
		/*7-02-2020*/
		/*.right-content h2 {
		    font-size: 36px !important;
		    line-height: 40px !important;
		    margin: 0px !important;
		    margin-bottom: 20px !important;
		    color: #005c9b;
		    text-transform: uppercase;
		    font-weight: 700;
		    font-family: Oswald;
		}*/
		.reviews_content {
		    font-size: 36px !important;
		    line-height: 40px !important;
		    margin: 0px !important;
		    margin-bottom: 20px !important;
		    color: #005c9b;
		    /*text-transform: capitalize;*/
		    font-weight: 400;
		    font-family: Oswald;
		}
		.right-content p {
		    font-size: 20px;
		    line-height: 30px;
		    text-transform: initial;
		}
		.right-content p span{
			color: #393939 !important;
			font-weight: 300;
			/*text-transform: capitalize;*/
		}
		
		.right-content p strong {
		    line-height: 30px;
		    color: #232323 !important;
		    font-size: 20px !important;
		    font-weight: 400;
		}
		.right-content p iframe {
		    border: 8px solid #fff !important;
		    width: 100%;
		}
		.left-content {
		    width: 100%;
		    position: relative;
		    max-width: 250px;
		}
		/*.post_category {
		    position: absolute;
		    top: 40px;
		}*/
		.left-content .catgory_post_title {
		    font-size: 24px;
		    color: #005c9b;
		    text-transform: uppercase;
		    font-family: Oswald;
		    margin: 0;
		}
		.left-content ul {
		    margin: 0;
		    margin-top: 20px;
		}
		.left-content .post_listing a{
		    font-size: 18px;
		    color: #393939;
		    font-weight: 400;
            line-height: 34px;
           /* text-transform: capitalize;*/
		}
		/*.left-content .post_listing a:hover {
		    text-decoration: none;
		}*/
		.location-page form {
		    display: flex;
		    align-items: center;
		}
		.left-content_icons ul li{
		    margin-left: -8px !important;
		    padding-left: 30px !important;
		    background: url("<?php echo get_template_directory_uri().'/images/orange_icon.png'?>") no-repeat !important;
		    font-size: 16px !important;
		}
		.commentlist {
		    margin: 0;
		    padding: 0;
		    display: flex;
		    justify-content: space-between;
		    list-style: none;
		    width: 100%;
		    padding: 0px 20px;
		}
		.comment-author.vcard img {
		    border-radius: 50%;
		    width: 55px;
		    height: 55px;
		    border: 4px solid #fff;
		}
		 .review_rate {
		    color: #ff9d11;
		    font-size: 8px;
		    letter-spacing: 2px;
		    position: absolute;
		    margin: -26px 0px 0px 80px;
		}
	    .commentlist .comment-meta.commentmetadata {
		    display: none;
		}
		.commentlist cite.fn {
		    font-weight: 500;
		    font-size: 18px;
		    color: #393939;
		    position: absolute;
		    top: 10px;
		    left: 80px;
		}
		.commentlist .comment-author.vcard {
		    position: relative;
		}
		.commentlist span.says {
		    display: none;
		}
		.commentlist p {
		    font-size: 18px;
		    color: #393939;
		   /* text-transform: capitalize;*/
		    font-family: Oswald;
		    margin: 20px 0px;	    
    		font-weight: 300;
		}
		.commentlist .comment {
		    margin: 0px 30px 0px 0px;
		    width: 100%;
		}
		/*.right-content .location-page #s {
		    background-image: unset;
		    border: 1px solid #fe690d;
		    border-radius: 1px;
		}*/
		.left-content.left-content_icons .post_category {
		    margin-top: 15px;
		}
		.left-content.left-content_icons .address_title {
		    font-size: 36px;
		    color: #005c9b;
		    font-family: Oswald;
		    margin: 0;
		    font-weight:400;
		}
		.left-content .address_container span {
		    font-size: 16px;
		    color: #4d4d4d;
		    font-weight: 400;
		}
		.left-content .location_flex {
		    display: flex;
		    align-items: center;
		    justify-content: flex-start;
		}
		.left-content .location_sub_title {
		    font-size: 18px;
		    color: #393939;
		    font-weight: 400;
		    padding: 10px 0px 10px 10px;
		    margin: 0;
		}
		.left-content .location_sub_title span{
			font-weight: 300;
		}
		.left-content .title_content{
			font-weight: 300;
		}
		.address_container {
		    padding: initial;
		    position: absolute;
		    top: 92.2%;
		    /*text-transform: capitalize;*/
		}
		.all_link .location_link {
		    background-color: #fff;
		    padding: 5px 25px;
		    border-radius: 20px;
		    font-size: 16px;
		    color: #393939;
		    font-weight: 400;
		    margin: 10px 6px 12px 0px;
		    transition: all .4s ease-in-out;
		}
		.all_link .location_link:hover {
		    background-color: #393939;
		    color: #fff;
		}
		.all_link {
		    margin-bottom: 10px;
		}
		.location_links_content {
		    display: block;
		    align-items: center;
		}
		.all_link .location_link:hover {
		    text-decoration: none;
		}
		.left-content .post_listing a:hover {
		    color: #fe690d;
		    text-decoration: blink;
		}
		.sub_title {
		    color: #232323 !important;
		    font-size: 28px !important;
		    text-transform: initial;
		    margin: 0px 0px 15px;
		    line-height: 30px;
		    font-weight: 400;
		    font-family: Oswald !important;
		}
		.main-title {
		    font-size: 36px !important;
		    margin: 0px 0px 30px !important;
		    /*text-transform: capitalize;*/
		    font-family: Oswald;
		    margin: 0;
		    font-weight:bold;
		}
		.location {
		    margin-top: 25px;
		}
		.personal_rate_chk .checked {
		    color: #ff9d11 !important;
		}
		.personal_rate_chk {
		    position: absolute;
		    top: 20px;
		}
		.personal_rate {
		    position: relative;
		}
		h2.reviews_content {
		    padding: 0px 20px;
		}
		.mid_cont1 {
		    padding: 0px 20px;
		}
		.linked_section {
		    display: flex;
		}
		.main_icon {
		    display: flex;
		    font-size: 16px;
		    font-weight: 700;
		    justify-content: space-between;
		    width: 100%;
		    align-items: flex-end;
		}
		.icons_section a:hover {
		    text-decoration: none;
		}
		.images_text img {
		    width: 100%;
		    object-fit: scale-down;
		    min-width: 200px;
		    height: 200px;
		}
		.icons_section .main_icon .sub_icons {
		    width: 175px;
		    background-color: #fff;
		    margin: 10px 0;
		    text-align: center;
		    padding: 10px;
		    border-radius: 10px;
		    transition: .4s;
		    height: 175px;
		    display: flex;
		    align-items: center;
		    text-align: center;
		    justify-content: center;
		    color: #393939 !important;
		}
		.icons_section .main_icon .sub_icons:hover {
			 color: #fe680d !important;
		}
		.icons_section .main_icon h5 {
		    font-size: 14px;
    		line-height: 20px;
		    font-weight: 400;
		    margin: 0;
		    padding-top: 5px;
		    text-transform: initial;
		   /* color: #393939;*/
		}

		.main_icon img {
		    width: 40px;
		}
		.main_icon .sub_icons:hover {
		    box-shadow: 0px 0px 20px 0px #bbb;
		    transition: .4s;
		    cursor: pointer;
		}
		/*.main_icon .sub_icons p:hover {
		    color: #fe680d !important;
		    text-decoration: none;
		}*/

		.flex_img_text p {
		    margin: 0;
		}
		.icons_section {
		    margin-bottom: 20px;
		}
	@media only screen and (min-width: 320px) and (max-width: 575px) {
		.content_ab.gap_news.newPageContent {
		    display: block;
		}
		.commentlist
		{
			display: block;
			min-width: 100%;
    		width: 100%;
    		padding:0px;
		}
		.left-content .location_icons {
		    max-width: 20px;
		}
		.content_ab.gap_news.newPageContent {
    		margin-top: 0px !important;
		}
		.address_container {
		    position: inherit;
		    top: unset;
		    /*text-transform: capitalize;
*/		}
		.commentlist .comment {
		    margin: 0;
		    padding: 20px 20px 0px;
		}
		.reviews_content{
			padding: 0px 10px;
		}
		.left-content ul{
			margin-bottom: 30px;
		}
		.commentlist .review_rate{
			margin: -23px 0px 0px 80px;
		}
		.commentlist cite.fn{
			top: 6px;
		}
		.main-title {
		    font-size: 28px !important;
		}
		.left-content {
		    max-width: 100%;
		}
		.right-content {
		    max-width: 100%;
		    padding: 0px;
		}
		.right-content p iframe {
		    max-height: 300px;
		}
		.mid_cont1 {
		   	 padding: 0;
		}
		.images_text p {
		    padding-left: 0;
		}
		.images_text {
		    display: inline-block;
		}
		.icons_section .main_icon .sub_icons {
		    width: 100%;
		}
		.main_icon {
		    display: block;
		}
	}
	@media only screen and (min-width: 576px) and (max-width: 767px) {
		.content_ab.gap_news.newPageContent {
		    display: block;
		}
		.commentlist
		{
			display: block;
			min-width: 100%;
    		width: 100%;
    		padding:0px;
		}
		.left-content .location_icons {
		    max-width: 20px;
		}
		.content_ab.gap_news.newPageContent {
    		margin-top: 0px !important;
		}
		.address_container {
		    position: inherit;
		    top: unset;
		   /* text-transform: capitalize;*/
		}
		.commentlist .comment {
		    margin: 0;
		    padding: 20px 20px 0px;
		}
		.reviews_content{
			padding: 0px 10px;
		}
		.left-content ul{
			margin-bottom: 30px;
		}
		.commentlist .review_rate{
			margin: -23px 0px 0px 80px;
		}
		.commentlist cite.fn{
			top: 6px;
		}
		.main-title {
		    font-size: 28px !important;
		}
		.left-content {
		    max-width: 100%;
		}
		.right-content {
		    max-width: 100%;
		    padding: 0px;
		}
		.right-content p iframe {
		    max-height: 300px;
		}
		.mid_cont1 {
		    padding: 0;
		}
		.images_text p {
		    padding-left: 0;
		}
		.images_text {
		    display: inline-block;
		}
		.icons_section .main_icon .sub_icons {
		    width: 100%;
		}
		.main_icon {
		    display: block;
		}

	}
	@media only screen and (min-width: 768px) and (max-width: 991px) {
		.content_ab.gap_news.newPageContent {
		    display: block;
		}
		.commentlist
		{
			display: block;
			min-width: 100%;
    		width: 100%;
		}
		.left-content .location_icons {
		    max-width: 20px;
		}
		.content_ab.gap_news.newPageContent {
    		margin-top: 0px !important;
		}
		.address_container {
		    position: inherit;
		    top: unset;
		    /*text-transform: capitalize;*/
		}
		.commentlist .comment {
		    margin: 0;
		    padding: 20px 20px 0px;
		}
		.reviews_content{
			padding: 0px 10px;
		}
		.left-content ul{
			margin-bottom: 30px;
		}
		.right-content {
		    max-width: 100%;
		}
		.right-content p iframe {
		    width: 100%;
		}
		.left-content {
		    max-width: 100%;
		}
		.mid_cont1 {
		    padding: 0;
		}
		.icons_section .main_icon .sub_icons {
		    width: 230px;
		    height: 195px;
		}
	}
	@media only screen and (min-width: 992px) and (max-width: 1199px) {
		.commentlist
		{
			min-width: 900px;
    		width: 100%;
		}
	}

	</style>
	<?php $postid1 = get_the_ID(); ?>
	<div class="cur_wrap">

		<div class="about_wrap1">

			<div class="mid_cont1">

        		<div class="content_ab gap_news newPageContent reviews_content" >
        			<div class="right-content">
        				<?php the_content(); ?>
        				
						
						<div class="clear"></div>
				        
        			</div>
        			
          			<div class="left-content left-content_icons">
          				<div class="serach">
          					<?php if ( is_active_sidebar( 'sidebar-location' ) ) : ?>
    							<?php dynamic_sidebar( 'sidebar-location' ); ?>
							<?php endif; ?>
						</div>
						<div class="post_category">
							<h2 class="catgory_post_title"> Personal Injury - Miami</h2>
							<div class="post_title_listing">
								<ul class="post_content">
									<? 
										$args = array(
    										'post_type' => 'post',
    										'posts_per_page' => -1,
    										'category_name'=>'Practice Areas',
										);
										$loop = new wp_Query($args);
										if($loop->have_posts())
										{
											while($loop->have_posts()) : $loop->the_post();
											?>

												<li class="post_listing"><a class="post_title" href="<?php echo get_permalink(); ?>"><?php echo the_title(); ?></a></li>
										<?php endwhile;
											wp_reset_postdata();		
										 
										}
										?>
								</ul>
							</div>
						</div>
						<div class="address_container">
							<h2 class="address_title">Leighton Law</h2>
							<div class="personal_rate">
								<span>Personal injury attorney</span>
								<div class="personal_rate_chk">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
								</div>
							</div>

							<div class="location location_flex">
								<img class="location_icon location_icons" src="<?php echo get_template_directory_uri().'/images/location.png' ?>">
								<p class="location_title location_sub_title">1401 Brickell Ave. #900<br> Miami, FL 33131</p>
							</div>
							<div class="contact location_flex">
								<img class="contact_icon location_icons" src="<?php echo get_template_directory_uri().'/images/call.png' ?>">
								<p class="contact_number location_sub_title">Phone: (888) 988-1774</p>
							</div>
							<div class="time location_flex">
								<img class="time_icon location_icons" src="<?php echo get_template_directory_uri().'/images/clock.png' ?>">
								<p class="time_content location_sub_title">8:30am – 6pm <span>(Monday To Friday)<span></p>
							</div>
								<p class="title_content location_sub_title">John Elliott Leighton is a board certified trial lawyer and the Managing Partner of Leighton Law, P.A., with offices in Miami and Orlando, Florida</p>
							
							<div class="location_links_content">
							<div class="linked_section">
							<div class="all_link">
								<a href="https://leightonlaw.com/practice-areas/" class="location_link">Practice Areas</a>
							</div>
							<div class="all_link">
								<a href="https://leightonlaw.com/about-leighton-law-personal-injury-trial-lawyers/" class="location_link">About</a>
							</div>
							</div>

							<div class="all_link">
								<a href="https://leightonlaw.com/verdicts-settlements/" class="location_link">Verdicts & Settlements</a>
							</div>
							</div>

							<!-- <div class="location_links_content">
							<div class="all_link">
								<a href="" class="location_link">Law Firm</a>
							</div>
							</div> -->
							
						</div>
          			</div>

       			</div>
       			<h2 class="reviews_content">Reviews</h2>
        				<ol class="commentlist">
						    <?php    
						    	//global $postid;
								
						        //Gather comments for a specific page/post 
						        $comments = get_comments(array(
						            'post_id' => $postid1,
						            'status' => 'approve' //Change this to the type of comments to be displayed
						        ));

						        //Display the list of comments
						        wp_list_comments(array(
						            'per_page' => 10, //Allow comment pagination
						            'reverse_top_level' => false //Show the latest comments at the top of the list
						        ), $comments);

						    ?>
						</ol>
						<div class="mid_cont1">
							<div class="cur_sli">
								<?php include( "php/logo-bar-newhome.php" ); ?>
							</div>
						</div>
       		</div>
       		<div class="clear"></div>
       		<div class="get-started" style="background-image: url('https://leightonlawstg.wpengine.com/wp-content/uploads/2019/03/form-background.png');">
							<div class="mid_cont1">
					        	<h2>LET’S GET STARTED</h2>
					        	<?php echo do_shortcode('[contact-form-7 id="7991" title="Get Started From"]')  ?>
					        </div>
				        </div>
       	</div>

    </div>


<?php get_footer(); ?>

