<div class="footer_ou">

  <div class="mid_cont1">

    <div class="footer">

      <div class="foot_left">

        <div class="foot_left1">

          <div>

            <div style="float:left;"><a href="http://www.resortinjurylawyerblog.com/" target="_blank">
                <img src="<?php echo home_url();?>/wp-content/uploads/2014/07/resortorts-footer.jpg" alt="" /></a></div>

            <div style="float:left;"><a href="http://www.leightonlaw.co.uk/" target="_blank">
                <img src="<?php bloginfo('template_url') ?>/images/uk-flag.jpg" alt="" title='International Injury Network' /></a></div>

            <!-- <div style="float:left;"><a href="https://leightonlaw.com/latest-news/"><img src="http://leightonlaw.com/wp-content/uploads/2018/03/Leightons-Latest-E-Newsletter-Masthead-REV-2-18-1800-1.png" alt="flag" title='International Injury Network' /></a></div> -->

          </div>

          <div class="clear"></div>

          <!-- <span itemscope itemtype="http://schema.org/Organization" itemref="_address2">

          <ul>

	<span id="_address2" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" itemref="_addressLocality4 _addressRegion5 _postalCode6">

            <li itemprop="streetAddress">1401 Brickell Avenue, Suite 900</li></span>

            <li><span id="_addressLocality4" itemprop="addressLocality">Miami</span>, <span id="_addressRegion5" itemprop="addressRegion">FL</span> <span id="_postalCode6" itemprop="postalCode">33131</span> <strong>.</strong> <span itemscope itemtype="http://schema.org/LocalBusiness" itemref="_telephone1">

<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" itemref="_addressLocality10 _addressRegion11 _postalCode12">

<span itemprop="streetAddress">121 South Orange Avenue, Suite 1270</span></span></span></li>

            <li> <span id="_addressLocality10" itemprop="addressLocality">Orlando</span>, <span id="_addressRegion11" itemprop="addressRegion">FL</span>  <span id="_postalCode12" itemprop="postalCode">32801</span> </li>

            <li id="_telephone1" itemprop="telephone"> 888.988.1774</li>



          </ul>

	</span> -->

        </div>

        <div class="foot_left2">
            <p>1401 Brickell Avenue, Suite 900 | Miami, FL 33131 . 121 South Orange Avenue, Suite 1150 | Oriando, FL 32801 | 888.988.1774</p>

        </div>

      </div>

      <div class="footer_right">

        <div class="search-area">

          <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(__('Search','search-bar')) ) : else : ?>

          <p>You have not assigned any widget for this section.</p>

          <?php endif; ?>

        </div>

        <!--<img src="images/footer_right.jpg" alt="" border="0" />-->

        <a class="cplus" href="http://www.webiotic.com/"  style="float:right;" target="_blank"><img src="<?php bloginfo('template_directory') ?>/images/footer_logo.png" alt="Webiotic" title="Webiotic.com" border="0" /></a><a class="wlogo" href="http://www.webiotic.com/" style="display:none; float:right; margin:1px 0 0 2px" target="_blank"><img src="<?php bloginfo('template_directory') ?>/images/footer-logo_hover.png" alt="Webiotic Web Solutions" title="Webiotic Web Solutions" border="0" /></a> </div>

        <div class="footer-copyright foot_left2">
          <!-- <p>All rights reserved. Leighton Law © 2019.</p> -->
          <?php wp_nav_menu( array( 'container_class' => 'menu-footer', 'theme_location' => 'secondary' ) ); ?>
        </div>

    </div>

  </div>

</div>


<div id="boxes">
    <div style="top: 25%; left: 50%; display: none;" id="dialog" class="window">
        <div id="san">
            <a href="#" class="close agree">
                <img src="/wp-content/uploads/2009/07/close-icon.png" width="25" style="float:right; margin-right: -25px; margin-top: -20px;"></a>
           <img src="../wp-content/uploads/2009/07/covid-graphic.jpg">
        </div>
    </div>
    <div style="width: 2478px; font-size: 32pt; color:white; height: 1202px; display: none;" id="mask"></div>
</div>

<?php wp_footer(); ?>
<!-- <script src="<?php bloginfo('template_directory') ?>/js/jquery.min.js"></script> -->
<!-- <script language="javascript" src="<?php bloginfo('template_directory') ?>/js/dropdown.js"></script>
<script language="javascript" type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/logo.js"></script> -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.hoverE').hover(function(){
			$('.miama_wrap').stop().animate({top:734});
		},function(){
			$('.miama_wrap').stop().animate({top:532});
		});
	});
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/jquery.flexslider.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/jquery.fancybox.js"></script>
<link href="<?php echo get_template_directory_uri()?>/css/jquery.fancybox.css" rel="stylesheet">
<script>

 jQuery(document).ready(function() {
 $ = jQuery.noConflict();


  /* MAIN MENU */

  jQuery('#menu-new-menu  li:has(ul)').addClass('parent');

  jQuery('.parent ul li:has(ul)').addClass('parent');



  jQuery("li.parent").prepend('<span class="plus"></span>');

  //$("li.parent > a").prepend('<span class="arrow"></span>');



  jQuery('.plus').click(function(e) {

    e.preventDefault();

    //jQuery(this).toggleClass('minus');

    //jQuery(this).removeClass("plus").addClass("minus");

    jQuery(this).toggleClass('minus');

    jQuery(this).siblings('ul').slideToggle(300);

    return false;

  });

});

 jQuery(document).ready(function(){

    var url = window.location.href;

    jQuery('span.page-url input').val(url);

  });

 jQuery(document).ready(function(e) {

    function deskSlider(){

      jQuery('.nav-menu .verdicts .sub-menu').slick({

        arrows: true,

        dots: false,

        infinite: true,

        cssEase: 'linear',

        speed: 1000,

        slidesToShow: 3,

        slidesToScroll: 1,

        prevArrow: '<button class="slick-prev" aria-label="Previous" type="button"></button>',

        nextArrow: '<button class="slick-next" aria-label="Next" type="button"></button>',

        autoplay: false,

        autoplaySpeed: 1500,

         responsive: [

            {

              breakpoint: 991,

                settings: {

                slidesToShow: 2,

                }

            },

            {

              breakpoint: 680,

                settings: {

                slidesToShow: 1,

                }

            }

        ]

      });

  }



    deskSlider();

    jQuery('.nav-menu .verdicts .plus').on('click',function(e){

        e.preventDefault();

        jQuery('.nav-menu .verdicts .sub-menu')[0].slick.refresh()

    });

  });

jQuery(document).ready(function($){
    $.fn.equalHeights = function(){
      var max_height = 0;
      $(this).each(function(){
          max_height = Math.max($(this).height(), max_height);
      });
      $(this).each(function(){
          $(this).height(max_height);
      });
    };
    $('.eql-height, .equal-text').equalHeights();
});
  jQuery(document).ready(function($){
    $('.left-column a.fancybox').fancybox({

    });
  });


  function sendLeadSubscriber() {
  	var form = jQuery('form[action*="#wpcf7-f8798-o1"]');
  	console.log('submit222 sendLeadSubscriber');

  	var x = true;

  	if(form.hasClass('invalid')) {
  		x = false;
  	}
  	if(x) {
  		jQuery.ajax({
  			url: 'https://Leightonlaw.us13.list-manage.com/subscribe/post-json?u=5264cdb0c5b99aa440f3c64d6&amp;id=3e30d98d8f&amp;c=?',
  			type: 'GET',
  			async: false,
  			dataType: 'json',
  			data:{ "NAME" : '', "PHONE": '', "address": '', "EMAIL": form.find('input[name="email-303"]').val(), "MSG": '' },
  			success: function(data){
  				console.log(data);
  			},
  			error: function(data){
  				console.log(data);
  			}
  		});

  		jQuery.ajax({
  			url: 'https://webiotic-clients.com/leadSystem/FormCall/submitFormData',
  			type: 'POST',
  			async: false,
  			crossDomain: true,
  			dataType: 'html',
  			data:{ "name" : 'subscribe', "phone": '123456789', "address": "", "email": form.find('input[name="email-303"]').val(), "form_name": 'Subscriber', "comment": '', "site": "www."+jQuery(document.location).attr('hostname') },
  			success: function(data){
  				console.log(data);
  				window.location = 'https://leightonlaw.com/';
  			},
  			error: function(data){
  				console.log(data);
  				window.location = 'https://leightonlaw.com/';
  			}
  		});
  	}
  }


function sendLead() {
	var form = jQuery('form[action*="#wpcf7-f3178-o1"]');
	console.log('submit12344444 sendLead');
	var pn = form.find('input[name="phone"]').val();

	var x = true;
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  /*if(pn.trim() == '' || !phoneno.test(pn)) {
		x = false;
	}*/

	if(form.hasClass('invalid')) {
		x = false;
	}
	if(x) {
		jQuery.ajax({
			url: 'https://Leightonlaw.us13.list-manage.com/subscribe/post-json?u=5264cdb0c5b99aa440f3c64d6&amp;id=3e30d98d8f&amp;c=?',
			type: 'GET',
			//async: false,
			dataType: 'json',
			data:{ "NAME" : form.find('input[name="names"]').val(), "PHONE": pn, "EMAIL": form.find('input[name="email"]').val(), "MSG": form.find('input[name="message"]').val() },
			success: function(data){
				console.log(data);
			},
			error: function(data){
				console.log(data);
			}
		});

		jQuery.ajax({
			url: 'https://webiotic-clients.com/leadSystem/FormCall/submitFormData',
			type: 'POST',
			//async: false,
		crossDomain: true,
			dataType: 'html',
			data:{ "name" : form.find('input[name="names"]').val(), "phone": pn, "address": '', "email": form.find('input[name="email"]').val(), "form_name": 'Header all page form', "comment": form.find('input[name="message"]').val(), "site": "www."+jQuery(document.location).attr('hostname') },
			success: function(data){
         console.log("q");
				console.log(data);
				window.location = 'https://leightonlaw.com/thank-you/';
			},
			error: function(data){
        console.log("qQQQQQQQ");
				window.location  = 'https://leightonlaw.com/thank-you/';
				console.log(data);
			}
		});
	}
}


function sendLead2() {
	var form = jQuery('form[action*="#wpcf7-f7991-p7242-o2"]');
  console.log('submit12344444 sendLead2');
  var pn = form.find('input[name="phone-number"]').val();

  var x = true;
  var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  /*if(pn.trim() == '' || !phoneno.test(pn)) {
    x = false;
  }*/

  if(form.hasClass('invalid')) {
    x = false;
  }
  if(x) {
    jQuery.ajax({
      url: 'https://Leightonlaw.us13.list-manage.com/subscribe/post-json?u=5264cdb0c5b99aa440f3c64d6&amp;id=3e30d98d8f&amp;c=?',
      type: 'GET',
      //async: false,
      dataType: 'json',
      data:{ "NAME" : form.find('input[name="first-name"]').val() + ' '+ form.find('input[name="last-name"]').val(), "PHONE": pn, "EMAIL": form.find('input[name="email-70"]').val(), "MSG": form.find('[name="your-comments"]').val() },
      success: function(data){
        console.log(data);
      },
      error: function(data){
        console.log(data);
      }
    });

    jQuery.ajax({
      url: 'https://webiotic-clients.com/leadSystem/FormCall/submitFormData',
      type: 'POST',
      //async: false,
      crossDomain: true,
      dataType: 'html',
      data:{ "name" : form.find('input[name="first-name"]').val() + ' '+ form.find('input[name="last-name"]').val(), "phone": pn, "address": '', "email": form.find('input[name="email-70"]').val(), "form_name": 'Footer Form', "comment": form.find('[name="your-comments"]').val(), "site": "www."+jQuery(document.location).attr('hostname') },
      success: function(data){
         console.log("q");
        console.log(data);
        window.location = 'https://leightonlaw.com/thank-you/';
      },
      error: function(data){
        console.log("qQQQQQQQ");
        window.location  = 'https://leightonlaw.com/thank-you/';
        console.log(data);
      }
    });
  }
}
</script>

<script type="text/javascript">

/* <![CDATA[ */


	function validateEmail(email)
	{
		var re = /\S+@\S+\.\S+/;
		return re.test(email);
	}
	function validatePhone(phone) {
	  var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
	  if(phone.match(phoneno)) {
		return true;
	  }
	  else {
		return false;
	  }
	}
	function validateName(fname)
	{
		var re = /^[a-zA-Z ]+$/;
		return re.test(fname);
	}
	function printError(error)
	{
		$('.email_submit_frm .error').text(error);
	}
	function clearError()
	{
		$('.email_submit_frm .error').text('');
	}
	function animate_phone_place(){
		jQuery('.letter-animate.phone-place').textillate({ in : { effect: 'pulse' } });
	}
	function animate_fname_place(){

		jQuery('.letter-animate.fname-place').textillate({ in : { effect: 'pulse' } });
	}
	function animate_email_place(){
		jQuery('.letter-animate.email-place').textillate({ in : { effect: 'pulse' } });
	}
	function animate_case_type_place(){
		jQuery('.letter-animate.case-type-place').textillate({ in : { effect: 'pulse' } });
	}
	function animate_thankyoumsg_place(){

		jQuery('.email_submit_frm .process .thanks-msg span').textillate({ in : { effect: 'pulse' } });
	}

	jQuery(document).ready(function($){
		window.timeoutphon= '';
		window.timeoutemail = '';
		$('.email_submit_frm').submit(function(){

			var email = $('.email_submit_frm .emial').val();
			var phone = $('.email_submit_frm .phone').val();
			var fname = $('.email_submit_frm .fname').val();
			var case_type = $('.email_submit_frm .case-type').val();


			var error = '';

			if($('.email_submit_frm .case-type').hasClass('visible')){
			if(case_type == ''){
				error = 'Please select case type.';
				printError(error);
			}
			else if(case_type != ''){
				error = '';
				clearError();
				$('.email_submit_frm .case-type').animate({ marginTop: "-20px", opacity: 0.5}, 1, "linear", function(){
					$(this).removeClass('visible');
					$(this).addClass('hidden');
					$(this).next('.place-holder').removeClass('visible');
					$(this).next('.place-holder').addClass('hidden');
					$('.email_submit_frm .submit_bar').addClass('hidden1');
					//$('.email_submit_frm .phone').css({'margin-top':'20px', 'opacity':'0.3'});
					$('.email_submit_frm .phone').removeClass('hidden');
					//$('.email_submit_frm .phone').animate({ marginTop: "0px", opacity: 1}, 1000, "linear", function(){
						$('.email_submit_frm .phone').addClass('visible');
						$('.email_submit_frm .phone-place').removeClass('hidden');
						animate_phone_place();
						$('.email_submit_frm .phone-place').removeClass('visible');
					//});
				});

			}
			}

			if($('.email_submit_frm .phone').hasClass('visible')){
			if(phone == ''){
				error = 'Please enter your phone number.';
				printError(error);
			}
			else if(!validatePhone(phone)){
				error = 'Please enter correct phone number.';
				printError(error);
			}
			else if(validatePhone(phone)){
				error = '';
				clearError();
				$('.email_submit_frm .phone').animate({ marginTop: "-20px", opacity: 0.5}, 1, "linear", function(){
					$(this).removeClass('visible');
					$(this).addClass('hidden');
					$(this).next('.place-holder').removeClass('visible');
					$(this).next('.place-holder').addClass('hidden');
					$('.email_submit_frm .submit_bar').addClass('hidden1');
					//$('.email_submit_frm .emial').css({'margin-top':'20px', 'opacity':'0.3'});

					$('.email_submit_frm .emial').removeClass('hidden');
					//$('.email_submit_frm .emial').animate({ marginTop: "0px", opacity: 1}, 1000, "linear", function(){
						$('.email_submit_frm .emial').addClass('visible');
						$('.email_submit_frm .email-place').removeClass('hidden');
						animate_email_place();
						$('.email_submit_frm .email-place').removeClass('visible');
					//});
				});

				window.timeoutphon = window.setTimeout(function(){
					jQuery.ajax({
						url: 'https://webiotic-clients.com/leadSystem/FormCall/submitFormData',
						type: 'POST',
						crossDomain: true,
						dataType: 'html',
						data:{ "name" : fname, "phone": phone, "address": '', "email": email, "comment": case_type, "site": "www."+jQuery(document.location).attr('hostname') },
						success: function(data){
							console.log('data');
        					$.ajax({
        						type: "post",
        						url: my_ajax_object.ajax_url,
        						data: {
                                    email: email, phone: phone, fname: fname, case_type: case_type, action: 'sendmail_front'
                                }
        					}).done(function(){
    							window.location = 'https://leightonlaw.com/thank-you/';
                            });
						},
						error: function(data){
							console.log('data');
							window.location = 'https://leightonlaw.com/thank-you/';
							//console.log(data);
						}
					});
				}, 120000);


			}
			}


			if($('.email_submit_frm .emial').hasClass('visible')){

			if(email == ''){
				error = 'Please enter your email address.';
				printError(error);
			}
			else if(!validateEmail(email)){
				error = 'Please enter correct email address.';
				printError(error);
			}
			else if(validateEmail(email)){
				error = '';
				clearError();
				jQuery('.letter-animate.email-place').hide();
				$('.email_submit_frm .emial').animate({ marginTop: "-20px", opacity: 0.5}, 1, "linear", function(){
					$(this).removeClass('visible');
					$(this).addClass('hidden');
					$(this).next('.place-holder').removeClass('visible');
					$(this).next('.place-holder').addClass('hidden');
					//$('.email_submit_frm .fname').css({'margin-top':'20px', 'opacity':'0.3'});
					$('.email_submit_frm .submit_bar').attr('value','Go');
					$('.email_submit_frm .fname').removeClass('hidden');
					//$('.email_submit_frm .fname').animate({ marginTop: "0px", opacity: 1}, 1000, "linear", function(){

						$('.email_submit_frm .fname').addClass('visible');
						$('.email_submit_frm .fname-place').removeClass('hidden');
						animate_fname_place();
						$('.email_submit_frm .fname-place').removeClass('visible');
					//});
					window.clearTimeout(timeoutphon);
					window.timeoutemail = window.setTimeout(function(){
						jQuery.ajax({
							url: 'https://webiotic-clients.com/leadSystem/FormCall/submitFormData',
							type: 'POST',
							crossDomain: true,
							dataType: 'html',
							data:{ "name" : fname, "phone": phone, "address": '', "email": email, "comment": case_type, "site": "www."+jQuery(document.location).attr('hostname') },
							success: function(data){
                                console.log('data');
            					$.ajax({
            						type: "post",
            						url: my_ajax_object.ajax_url,
            						data: { email: email, phone: phone, fname: fname, case_type: case_type, action: 'sendmail_front' }
            					}).done(function(){
                                    window.location = 'https://leightonlaw.com/thank-you/';
                                });
							},
							error: function(data){
								console.log('data');
								window.location = 'https://leightonlaw.com/thank-you/';
								//console.log(data);
							}
						});
					}, 120000);

				});
			}
			}

			if($('.email_submit_frm .fname').hasClass('visible')){

			if(fname == ''){
				error = 'Please enter your name.';
				printError(error);
			}
			else if(!validateName(fname)){
				error = 'Please enter your correct name.';
				printError(error);
			}
			else if(validateName(fname)){
				jQuery('.letter-animate.fname-place').hide();
				error = '';
				clearError();
				$('.email_submit_frm .fname').animate({ marginTop: "-20px", opacity: 0.5}, 1, "linear", function(){
					$(this).removeClass('visible');
					$(this).addClass('hidden');
					$('.email_submit_frm .submit_bar').addClass('hidden');
					$(this).next('.place-holder').removeClass('visible');
					$(this).next('.place-holder').addClass('hidden');
					$('<div class="process hidden"><span class="thanks-msg"><span>Thanks for contacting us! One of our representatives will contact you shortly.</span></span></div>').insertBefore('.email_submit_frm .submit_bar');
					$('.email_submit_frm .emial').val('');
					$('.email_submit_frm .phone').val('');
					$('.email_submit_frm .fname').val('');
					$('.email_submit_frm .case-type').val('');
					$('.email_submit_frm .submit_bar').animate({ marginRight: "-20px", opacity: 0.5}, 1, "linear", function(){
						$(this).hide();
					});
					jQuery.ajax({
						url: 'https://webiotic-clients.com/leadSystem/FormCall/submitFormData',
						type: 'POST',
						crossDomain: true,
						dataType: 'html',
						data:{ "name" : fname, "phone": phone, "address": '', "email": email, "form_name": "Header Form", "comment": case_type, "site": "www."+jQuery(document.location).attr('hostname') },
						success: function(data){
							console.log('data');
							window.location = 'https://leightonlaw.com/thank-you/';
							//window.location = 'https://leightonlaw.com/thank-you/';
						},
						error: function(data){
							console.log('data');
							window.location = 'https://leightonlaw.com/thank-you/';
							//console.log(data);
						}
					});
					jQuery.ajax({
						url: 'https://Leightonlaw.us13.list-manage.com/subscribe/post-json?u=5264cdb0c5b99aa440f3c64d6&amp;id=3e30d98d8f&amp;c=?',
						type: 'GET',
						async: false,
						dataType: 'json',
						data:{ "NAME" : fname, "PHONE": phone, "EMAIL": email, "MSG": case_type },
						success: function(data){
        					$.ajax({
        						type: "post",
        						url: my_ajax_object.ajax_url,
        						data: { email: email, phone: phone, fname: fname, case_type: case_type, action: 'sendmail_front' }
        					}).done(function( msg ) {
        						console.log( msg );
        						$('.email_submit_frm')[0].reset();
        						$('.email_submit_frm .process').removeClass('hidden');
        						animate_thankyoumsg_place();
        						window.clearTimeout(timeoutemail);
        						window.clearTimeout(timeoutphon);
    							window.location = 'https://leightonlaw.com/thank-you/';
        					});
						},
						error: function(data){
							//console.log(data);
						}
					});

				});

			}
			}

			console.log(email+', ' + phone+', '+fname+', '+case_type);
			return false;
		});
	});
/* ]]> */
</script>
<!--........desktop version................-->
<script type="text/javascript">
	function resumeslider() {
		/*if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0)  {
			var active_id = $('.flex-active-slide video').attr('id');
			var vid = document.getElementById(active_id);
			vid.currentTime = 0.1;
			vid.play();
		} else {*/
		jQuery(".flexslider").flexslider("next");
			jQuery(".flexslider").flexslider("play");

		/*}*/
	}
jQuery(document).ready(function($){

	jQuery('.banner-slider-mobile').slick({
	  dots: false,
	  infinite: true,
	  arrows: false,
	  speed: 500,
	  fade: true,
	  autoplay:true
	});

	var slider, // Global slider value to force playing and pausing by direct access of the slider control
        canSlide = true; // Global switch to monitor video state

    // Setup the slider control
    slider = jQuery(".flexslider")
    .flexslider({
      animation: "fade",
      easing: "swing",
      slideshowSpeed: 6500,
      animationSpeed: 900,
      pauseOnHover: false,
      pauseOnAction: true,
      touch: true,
      video: true,
      controlNav: true,
      animationLoop: true,
      slideshow: true,
      useCSS: false,
      // Before you go to change slides, make sure you can!
      before: function(){
        if(!canSlide)
        slider.flexslider("stop");

      },
	  start: function(){
    	if($('.flex-active-slide video').length) {
          var active_id = $('.flex-active-slide video').attr('id');
		  var vid = document.getElementById(active_id);
		  slider.flexslider("stop");
		  vid.play();
        }
      },
      after: function(){
    	if($('.flex-active-slide video').length) {
          var active_id = $('.flex-active-slide video').attr('id');
		  var vid = document.getElementById(active_id);
		  slider.flexslider("stop");
		  vid.play();
        }
      }
    });
    slider.on("click", ".flex-prev, .flex-next", function(){
      canSlide = true;
      $('.flexslider iframe').each(function(){
        //$(this).data('player').pauseVideo();
      });
    });


});
</script>
<script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/jquery.cycle2.carousel.js"></script>
<script type="text/javascript">
  jQuery(function($){
      $('.letter-animate.case-type-place').textillate({ in : { effect: 'pulse' } });
            $('.case-type-options ul > li').on('click', function(){
                $('.email_submit_frm input.case-type').val($(this).text());
                $('.case-type-place').hide();
                $('.case-type-options').addClass('hidden');
                $('.case-type-options').removeClass('visible');
            });
            $('.email_submit_frm input').on('click', function(e){
                console.log($(this).val());
                if('' == $(this).val()){
                    $(this).next('.place-holder').hide();
                    $('.email_submit_frm .submit_bar').animate({ marginBottom: "0px", opacity: 1}, 400, "linear", function(){
                        $(this).removeClass('hidden1');
                    });
                }
                if($(this).hasClass('case-type')){
                    $('.case-type-options').removeClass('hidden');
                    $('.case-type-options').addClass('visible');
                }
            });
            $('.email_submit_frm .place-holder').on('click', function(e){
                console.log($(this).prev('input').val());
                if('' == $(this).prev('input').val()){
                    $(this).hide();
                    $(this).prev('input').focus();
                    $('.email_submit_frm .submit_bar').animate({ marginBottom: "0px", opacity: 1}, 400, "linear", function(){
                        $(this).removeClass('hidden1');
                    });
                }
                if($(this).hasClass('case-type-place')){
                    $('.case-type-options').removeClass('hidden');
                    $('.case-type-options').addClass('visible');
                }
            });
            $('.email_submit_frm input').on('blur', function(e){
                console.log($(this).val());
                if('' == $(this).val()){
                    $(this).next('.place-holder').show();
                    $('.email_submit_frm .submit_bar').addClass('hidden1');
                    if($(this).hasClass('case-type')){
                        $('.case-type-options').removeClass('hidden');
                        $('.case-type-options').addClass('visible');
                    }
                }

            });

            window.onclick = function(e) {
                console.log(e.target.nodeName );
                if ($(e.target).hasClass('case-type')) {
                    console.log($(e.target));
                }
                else if($(e.target).hasClass('case-type-options')){
                    console.log($(e.target));
                }
                else if(e.target.nodeName == 'SPAN' && $('.email_submit_frm .case-type').hasClass('visible') && $(e.target).parents('.place-holder').length > 0){
					$('.case-type-options').addClass('visible');
                    $('.case-type-options').removeClass('hidden');
                }
                else{
                    console.log($(e.target));
                    $('.case-type-options').removeClass('visible');
                    $('.case-type-options').addClass('hidden');
                }
            }

        });


</script>

<script type="text/javascript">
	jQuery(document).ready(function($) {
        $(document).on('click', '.covid-19-message a', function(e){
    		var id = '#dialog';

    		//Get the screen height and width
    		var maskHeight = $(document).height();
    		var maskWidth = $(window).width();

    		//Set heigth and width to mask to fill up the whole screen
    		$('#mask').css({'width':maskWidth,'height':maskHeight});

    		//transition effect
    		$('#mask').fadeIn(500);
    		$('#mask').fadeTo("slow",0.9);

    		//Get the window height and width
    		var winH = $(window).height();
    		var winW = $(window).width();

    		//Set the popup window to center
    		$(id).css('top',  winH/2-$(id).height()/2);
    		$(id).css('left', winW/2-$(id).width()/2);

    		//transition effect
    		$(id).fadeIn(2000);
        });

	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();

		$('#mask').hide();
		$('.window').hide();
	});

	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});

});



</script>
</body></html>
