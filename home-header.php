<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
	<link rel="icon" href="<?php bloginfo('template_directory'); ?>/favicon.png" type="image/x-icon" />
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="google-site-verification" content="tywopIqpjkjV5jSDfFz7ZyrLEeMvhZKTHUbgag2htiE" />
<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?>
</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/print.css" type="text/css" media="print" />
	<link href='//fonts.googleapis.com/css?family=Roboto|Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'/>
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<?php /*?>
	<!-- <script src="<?php bloginfo('template_directory') ?>/js/jquery.min.js"></script> -->
	<!-- <script language="javascript" src="<?php bloginfo('template_directory') ?>/js/dropdown.js"></script>
	<script language="javascript" type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/logo.js"></script> -->
	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script> -->
	<?php */?>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.hoverE').hover(function(){
				$('.miama_wrap').stop().animate({top:734});
			},function(){
				$('.miama_wrap').stop().animate({top:532});
			});
		});
	</script>
	<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-52534617-1', 'auto');
  ga('send', 'pageview');
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-52534617-1']);
  _gaq.push(['_trackPageview']);
</script>
<meta name="google-site-verification" content="tywopIqpjkjV5jSDfFz7ZyrLEeMvhZKTHUbgag2htiE" />
<!--Start of Zopim Live Chat Script
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){
z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?4BJvipvC5a4HDnVfR1s9DQPrStkoSqSs';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
 End of Zopim Live Chat Script-->
 <script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0051/4340.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W9ZS3JF');</script>
<!-- End Google Tag Manager -->
</head>
<body class="homepage">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W9ZS3JF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php include ('Mobile-Detect/Mobile_Detect.php');
//echo get_template_directory_uri();
$detect = new Mobile_Detect;
?>
<div class="right_contant_new lg-sdhelp-cover">
<a href="<?php bloginfo('url'); ?>/contact/" class="contt_h lg-sdhelp" >Talk to Us</a>
<a href="<?php bloginfo('url'); ?>/blog/" class="latest_h lg-sdhelp" >Latest News</a>
</div>
<?php if( $detect->isMobile() && !$detect->isTablet() ){?>
<!--........mobile version................-->
<div class="bg_outer2">
<div class="mid_cont">
<div class="top_head">
<div class="logo"><a href="<?php bloginfo('url') ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="Logo" /></a></div>
<div class="right_head">
<div class="florida_cont">
<h2>Florida personal injury lawyers </h2>
<h3>talk to an attorney  <span><a href="tel:8883950001">888.395.0001</a></span></h3>
</div>
<div class="header_nav_container" >
<div class="nav-area tod">
<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
</div>
</div>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
<div class="slider_conmt">
<?php
    echo do_shortcode("[metaslider id=30]");
?>
</div>
<?php include( "php/social-links.php" ); ?>
<div class="miama_wrap">
<div class="left_shadow"></div>
<div class="mim_wrap">
<div class="left_mim">
<img src="<?php bloginfo('template_directory'); ?>/images/office1.png" alt="Office" />
<div class="clear"></div>
<h2>Miami Office</h2>
<p>1401 Brickell Avenue, Suite 900<br />
Miami, FL 33131</p>
<p>Toll Free: <a href="tel:8883950001">888.395.0001</a><br />
Phone: <a href="tel:3053473151">305.347.3151</a><br />
Fax: <a href="#">305.675.0123</a></p>
<a class="left_mim_link" target="_blank" href="https://www.google.com/maps/place/1401+Brickell+Ave+%23900/@25.7600611,-80.1918411,17z/data=!3m1!4b1!4m2!3m1!1s0x88d9b680dfd0913f:0xad57e84687bfe30b">view map</a>
</div>
<div class="mid_mim">
<img src="<?php bloginfo('template_directory'); ?>/images/office1.png" alt="Office" />
<div class="clear"></div>
<h2>Orlando Office</h2>
<p>121 South Orange Avenue
Suite 1150, Orlando, FL 32801</p>
<p>Toll Free: <a href="tel:3053473151">888.395.0001</a><br />
Phone: <a href="tel:3053473151">407.384.8004</a></p>
<p></p>
<a class="mid_mim_link" target="_blank" href="https://www.google.com/maps/place/121+S+Orange+Ave+%231150/@28.541108,-81.378705,17z/data=!3m1!4b1!4m2!3m1!1s0x88e77afe775c318f:0x66e1b4bb8afbd4ca" style="margin-top:21px;">view map</a>
</div>
<div class="right_mim_con">
<img src="<?php bloginfo('template_directory'); ?>/images/consult.png" alt="Office" />
<div class="clear"></div>
<h2>get a free consultation</h2>
<?php echo do_shortcode('[contact-form-7 id="3178" title="Consultation"]'); ?>
<!--<form action="" method="post">
<input type="text" value="Name" name="" class="textfeild" />
<input type="text" value="Phone" name="" class="textfeild" />
<input type="text" value="Email" name="" class="textfeild" />
<input type="submit" value="Submit" name="" class="right_mim_con_btn" />
</form>-->
</div>
</div>
<div class="right_shadow"></div>
</div>
</div>
<div class="clear"></div>
</div>
<!--........mobile version................-->
<?php } else{ ?>
<!--........desktop version................-->
<div class="bg_outer">
<div class="mid_cont">
<div class="top_head">
<div class="logo">

	<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="Logo" /></a></div>
<div class="right_head">
<div class="florida_cont" style='position:relative;'>
<h2 style='position:absolute;top:-57px;right:-10px;background:#FE680D;color:white;padding:4px 10px;border:1px solid #FE680D;border-radius:4px;display:none;'><a style='font-size:14px;color:white;text-shadow:0 0 2px rgba(50,50,50,.9);' href='/march-2015-seminar'>Negligent Security Seminar | March 2015</a></h2>
<h2>Florida personal injury lawyers </h2>
<h3>talk to an attorney  <span><a href="tel:8889881774">888.988.1774</a></span></h3>
</div>
<div class="nav-area">
<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
</div>
</div>
</div>
<?php include( "php/social-links.php" ); ?>
<div class="clear"></div>
</div>
<div style="width:1000px; margin:0 auto; position:relative">
<div class="miama_wrap">
<div class="left_shadow"></div>
<div class="mim_wrap">
<div class="left_mim">
<img src="<?php bloginfo('template_directory'); ?>/images/office1.png" alt="Office" />
<div class="clear"></div>
<h2>Miami Office</h2>
<p>1401 Brickell Avenue, Suite 900<br />
Miami, FL 33131</p>
<p>Toll Free: <a href="tel:8889881774">888.988.1774</a><br />
Phone: <a href="tel:3053473151">305.347.3151</a><br />
Fax: <a href="#">305.675.0123</a></p>
<a class="left_mim_link" target="_blank" href="https://www.google.com/maps/place/1401+Brickell+Ave+%23900/@25.7600611,-80.1918411,17z/data=!3m1!4b1!4m2!3m1!1s0x88d9b680dfd0913f:0xad57e84687bfe30b" style="margin-top:6px;">view map</a>
</div>
<div class="mid_mim">
<img src="<?php bloginfo('template_directory'); ?>/images/office1.png" alt="Office" />
<div class="clear"></div>
<h2>Orlando Office</h2>
<p>121 South Orange Avenue<br />
Suite 1150, Orlando, FL 32801</p>
<p>Toll Free: <a href="tel:8889881774">888.988.1774</a><br />
Phone: <a href="tel:4073848004">407.384.8004</a></p>
<p></p>
<a class="mid_mim_link" target="_blank" href="https://www.google.com/maps/place/121+S+Orange+Ave+%231150/@28.541108,-81.378705,17z/data=!3m1!4b1!4m2!3m1!1s0x88e77afe775c318f:0x66e1b4bb8afbd4ca" style="margin-top:29px;">view map</a>
</div>
<div class="right_mim_con">
<img src="<?php bloginfo('template_directory'); ?>/images/consult.png" alt="Office" />
<div class="clear"></div>
<h2>get a free consultation</h2>
<?php echo do_shortcode('[contact-form-7 id="3178" title="Consultation"]'); ?>
<!--<form action="" method="post">
<input type="text" value="Name" name="" class="textfeild" onfocus="if (this.value==this.defaultValue) this.value = ''"
onblur="if (this.value=='') this.value = this.defaultValue" />
<input type="text" value="Phone" name="" class="textfeild" onfocus="if (this.value==this.defaultValue) this.value = ''"
onblur="if (this.value=='') this.value = this.defaultValue" />
<input type="text" value="Email" name="" class="textfeild" onfocus="if (this.value==this.defaultValue) this.value = ''"
onblur="if (this.value=='') this.value = this.defaultValue" />
<input type="submit" value="Submit" name="" class="right_mim_con_btn" />
</form>-->
</div>
</div>
<div class="right_shadow"></div>
</div>
</div>
<div class="hoverE">
<div style="width:100%; margin:0 auto; padding:0px; text-align:center; overflow:hidden;"><?php
    echo do_shortcode("[metaslider id=2775]");
?></div>
</div>
<div class="clear"></div>
</div>
<!--........desktop version................-->
<?php } ?>
<script type="text/javascript">
jQuery(document).ready(function($){
	$(".cycle-slideshow").attr("data-cycle-fx","carousel");
	$(".cycle-slideshow").attr("data-cycle-slides","> div")
	$(".cycle-slideshow").attr("data-cycle-carousel-visible","5");
	$(".cycle-slideshow").attr("data-cycle-carousel-fluid","true");
	$(".cycle-slideshow").attr("data-cycle-next",".next");
	$(".cycle-slideshow").attr("data-cycle-prev",".prev");
	$(".cycle-slideshow").attr("data-cycle-timeout","0");

});
</script>
<script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/jquery.cycle2.carousel.js"></script>
