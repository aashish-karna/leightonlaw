<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/print.css" type="text/css" media="print" />
	<link href='//fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'/>
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script language="javascript" src="<?php bloginfo('template_directory') ?>/js/dropdown.js"></script>
	<script language="javascript" type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/logo.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.hoverE').hover(function(){
				$('.miama_wrap').stop().animate({top:734});
			},function(){
				$('.miama_wrap').stop().animate({top:532});
			});
		});
	</script>
	<?php wp_head(); ?>
</head>
<body>
<div class="right_contant_new">
<a href="<?php bloginfo('url'); ?>/contact/" class="contt_h" ></a>
<a href="<?php bloginfo('url'); ?>/latest-news/" class="latest_h" ></a>
</div>
<!--........desktop version................-->
<div class="bg_outer">
<div class="mid_cont">
<div class="top_head">
<div class="logo"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="Logo" /></a></div>
<div class="right_head">
<div class="florida_cont">
<h2>Florida personal injury lawyers </h2>
<h3>talk to an attorney  <span>888.395.0001</span></h3>
</div>
<div class="nav-area">
<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
</div>
</div>
</div>
<?php include( "php/social-links.php" ); ?>
<div class="clear"></div>
</div>
<div style="width:1000px; margin:0 auto; position:relative">
<div class="miama_wrap">
<div class="left_shadow"></div>
<div class="mim_wrap">
<div class="left_mim">
<img src="<?php bloginfo('template_directory'); ?>/images/office1.png" alt="Office" />
<div class="clear"></div>
<h2>Miami Office</h2>
<p>1401 Brickell Avenue, Suite 900<br /> 
Miami, FL 33131</p>
<p>Toll Free: 888.395.0001<br />
Phone: 305.347.3151<br />
Fax: 305.675.0123</p>
<a target="_blank" href="https://www.google.com/maps/place/1401+Brickell+Ave+%23900/@25.7600611,-80.1918411,17z/data=!3m1!4b1!4m2!3m1!1s0x88d9b680dfd0913f:0xad57e84687bfe30b">view map</a>
</div>
<div class="mid_mim">
<img src="<?php bloginfo('template_directory'); ?>/images/office1.png" alt="Office" />
<div class="clear"></div>
<h2>Orlando Office</h2>
<p>121 South Orange Avenue<br /> 
Suite 1150, Orlando, FL 32801</p>
<p>Toll Free: 888.395.0001<br />
Phone: 407.384.8004</p>
<p></p>
<a target="_blank" href="https://www.google.com/maps/place/121+S+Orange+Ave+%231150/@28.541108,-81.378705,17z/data=!3m1!4b1!4m2!3m1!1s0x88e77afe775c318f:0x66e1b4bb8afbd4ca" style="margin-top:21px;">view map</a>
</div>
<div class="right_mim_con">
<img src="<?php bloginfo('template_directory'); ?>/images/consult.png" alt="Office" />
<div class="clear"></div>
<h2>get a free consultation</h2>

<?php echo do_shortcode('[contact-form-7 id="3178" title="Consultation"]'); ?>

<!--<form action="" method="post">
<input type="text" value="Name" name="" class="textfeild" onfocus="if (this.value==this.defaultValue) this.value = ''"
onblur="if (this.value=='') this.value = this.defaultValue" />
<input type="text" value="Phone" name="" class="textfeild" onfocus="if (this.value==this.defaultValue) this.value = ''"
onblur="if (this.value=='') this.value = this.defaultValue" />
<input type="text" value="Email" name="" class="textfeild" onfocus="if (this.value==this.defaultValue) this.value = ''"
onblur="if (this.value=='') this.value = this.defaultValue" />
<input type="submit" value="Submit" name="" class="right_mim_con_btn" />
</form>-->
</div>
</div>
<div class="right_shadow"></div>
</div>
</div>
<div class="hoverE">
<div style="width:100%; margin:0 auto; padding:0px; text-align:center; overflow:hidden;"><?php 
    echo do_shortcode("[metaslider id=2775]"); 
?></div>
</div>
<div class="clear"></div>
</div>
<!--........desktop version................-->
<!--........mobile version................-->
<div class="bg_outer2">
<div class="mid_cont">
<div class="top_head">
<div class="logo"><a href="<?php bloginfo('url') ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="Logo" /></a></div>
<div class="right_head">
<div class="florida_cont">
<h2>Florida personal injury lawyers </h2>
<h3>talk to an attorney  <span>888.395.0001</span></h3>
</div>
<div class="header_nav_container" >
<div class="nav-area">
<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
</div>
</div>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
<div class="slider_conmt">
<?php 
    echo do_shortcode("[metaslider id=30]"); 
?>
</div>
<?php include( "php/social-links.php" ); ?>
<div class="miama_wrap">
<div class="left_shadow"></div>
<div class="mim_wrap">
<div class="left_mim">
<img src="<?php bloginfo('template_directory'); ?>/images/office1.png" alt="Office" />
<div class="clear"></div>
<h2>Miami Office</h2>
<p>1401 Brickell Avenue, Suite 900<br /> 
Miami, FL 33131</p>
<p>Toll Free: 888.395.0001<br />
Phone: 305.347.3151<br />
Fax: 305.675.0123</p>
<a target="_blank" href="https://www.google.com/maps/place/1401+Brickell+Ave+%23900/@25.7600611,-80.1918411,17z/data=!3m1!4b1!4m2!3m1!1s0x88d9b680dfd0913f:0xad57e84687bfe30b">view map</a>
</div>
<div class="mid_mim">
<img src="<?php bloginfo('template_directory'); ?>/images/office1.png" alt="Office" />
<div class="clear"></div>
<h2>Orlando Office</h2>
<p>121 South Orange Avenue
Suite 1150, Orlando, FL 32801</p>
<p>Toll Free: 888.395.0001<br />
Phone: 407.384.8004</p>
<p></p>
<a target="_blank" href="https://www.google.com/maps/place/121+S+Orange+Ave+%231150/@28.541108,-81.378705,17z/data=!3m1!4b1!4m2!3m1!1s0x88e77afe775c318f:0x66e1b4bb8afbd4ca" style="margin-top:21px;">view map</a>
</div>
<div class="right_mim_con">
<img src="<?php bloginfo('template_directory'); ?>/images/consult.png" alt="Office" />
<div class="clear"></div>
<h2>get a free consultation</h2>

<?php echo do_shortcode('[contact-form-7 id="3178" title="Consultation"]'); ?>

<!--<form action="" method="post">
<input type="text" value="Name" name="" class="textfeild" />
<input type="text" value="Phone" name="" class="textfeild" />
<input type="text" value="Email" name="" class="textfeild" />
<input type="submit" value="Submit" name="" class="right_mim_con_btn" />
</form>-->
</div>
</div>
<div class="right_shadow"></div>
</div>
</div>
<div class="clear"></div>
</div>
<!--........mobile version................-->
<script type="text/javascript">
$(document).ready(function(){
	$(".cycle-slideshow").attr("data-cycle-fx","carousel");
	$(".cycle-slideshow").attr("data-cycle-slides","> div")
	$(".cycle-slideshow").attr("data-cycle-carousel-visible","5");
	$(".cycle-slideshow").attr("data-cycle-carousel-fluid","true");
	$(".cycle-slideshow").attr("data-cycle-next",".next");
	$(".cycle-slideshow").attr("data-cycle-prev",".prev");
	$(".cycle-slideshow").attr("data-cycle-timeout","0");
	
}); 
</script>
<script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/jquery.cycle2.carousel.js"></script>