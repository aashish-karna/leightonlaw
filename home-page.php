<?php /* Template Name: home-page */ ?>
<?php
include('home-header.php'); ?>

<script type="text/javascript">
    $( document ).ready( function() {
        $( "#menu-new-menu > li > ul > li" ).mouseenter( function() {
            var menu = $( this );
            $( ".sub-menu", menu ).css( { top: - menu.offset().top + $( "#menu-new-menu" ).offset().top + $( "#menu-new-menu > li" ).height() } );
            console.log( menu.offset().top );
        });
    });
</script>

<div class="cur_wrap">
	<div id="content">
		
		<div class="mid_cont1">
			<div class="cur_sli">
				<?php include( "php/logo-bar.php" ); ?>
			</div>
			<div class="sero_cont">
				<?php the_post(); the_content(); ?>
				<?php get_sidebar( 'above' ); ?>
			</div>
		</div>
		<div class="clear"></div>
		
	</div>
	<div class="clear"></div>
</div>
<?php include('footer-home.php'); ?>