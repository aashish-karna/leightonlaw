<?php include('blog-header.php'); ?>

<div id="content">
<style type="text/css">
.blog_main_wrap {
    background: #FFF;
    box-shadow: 1px 1px 12px 1px #CCC;
} 
.blog_wrap {
    padding: 30px;
}
.main_wrap {
    margin-bottom: 35px;
}
.blog_main_wrap {
  overflow: hidden;
  clear: both;
}

.blog_date {
  float: right;
  padding-right: 20px;
  padding-top: 20px;
  color: #606060;
  font-weight: 300;
  letter-spacing: 0.5px;
}
.blog_wrap h2 {
  font-size: 36px;
  margin: 23px 0 0px 0px;
  line-height: 45px;
}  
.blog_wrap p {
  color: #606060;
  font-weight: 300;
  letter-spacing: 0.5px;
  font-size: 24px;
  line-height: 28px;
  margin-bottom:0;
}
.loadmore.load-more {
  text-align: center;
  margin-bottom: 90px;
}
.loadmore.load-more p {
  cursor: pointer;
  background: #ff670e;
  display: inline-block;
  padding: 12px 60px;
  border-radius: 2px;
  color: #FFFF;
  font-family: Oswald;
  font-weight: 300;
  letter-spacing: 0.5px;
}
.newpagetemplate .newTemplatePageForm {
    position: absolute;
    top: 326px;
    left: 50%;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
    width: 85%;
    z-index:500;
}
.newTemplatePageForm h2 {
    font-size: 32px;
    font-weight: 900 !important;
    font-family: Oswald;
}
#category-dropdown {
  background-image: url(../wp-content/themes/leighton/images/angle-arrow-down.png);
  background-repeat: no-repeat;
  -webkit-appearance: none;
  outline: none;
  background-position: right 10px;
  border: none;
  border-bottom: 1px solid #FFF;
  font-size: 32px;
  font-family: Oswald;
  font-weight: 900;
  text-align: center;
  color: #FFF;
  padding-right: 15px;
  display: inline-block;
  background-color: transparent;
  padding-bottom: 10px;
  line-height: 40px;
  border-radius: 0;
  max-width: 250px;
}
#category-dropdown option {
    color: #515151;
    font-size: 20px;
}

.content_ab.gap_news.newPageContent {
    margin-top: -210px;
}


.bg_outer_about::before {
  display: table;
}

#content:after{
  content: '';
  clear: both;
  display: table;
}
.get-started {
    display: block;
    clear: both;
    background-position: center center;
    background-repeat: no-repeat;
    padding: 80px 0 55px;
    background-size: cover;
    width: 100%;
}
.get-started .mid_cont1 {
    width: 1005px;
    margin: 0 auto;
    position: relative;
}
.get-started h2 {
    color: #ffffff;
    font-family: "Open Sans";
    font-size: 50px;
    font-weight: 700;
    text-transform: uppercase;
    text-align: center;
    margin-bottom: 50px;
}
.get-started .form-col {
    margin-bottom: 15px;
    max-width: 33.33%;
    flex: 0 0 33.33%;
    padding: 0 15px;
    box-sizing: border-box;
}
.get-started .form-col.col-textarea {
    max-width: 99.7%;
    flex: 0 0 99.7%;
}
.get-started .form-submit-footer-btn.form-col-full {
    margin: 15px auto 0;
}
.get-started .started-form {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    margin: 0 -15px;
}
.get-started .form-col .first-name:before, .form-col .last-name:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/first_name.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 12px;
    height: 14px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}
.get-started .form-col .email-70:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/email_box.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 19px;
    height: 15px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}
.get-started .form-col .phone-number:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/phone_number.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 11px;
    height: 15px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}
.get-started .form-col .type-service:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/type_service.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 22px;
    height: 21px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}
.get-started .form-col .about-us:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/about_us.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 10px;
    height: 17px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}
.get-started .form-col .your-comments:before {
    content: '';
    background-image: url(../wp-content/themes/leighton/images/comment.png);
    background-repeat: no-repeat;
    position: absolute;
    width: 16px;
    height: 22px;
    display: block;
    top: 2px;
    left: 0;
    background-size: cover;
}

.get-started .form-col input, .form-col textarea, .form-col select {
    background: transparent;
    box-shadow: none;
    border-left: none;
    border-radius: 0;
    border-right: none;
    border-top: none;
    border-bottom: 1px solid #fff;
    padding-bottom: 33px;
    color: #ffffff;
    font-family: "Open Sans";
    font-size: 14px;
    font-weight: 400;
    max-width: 100%;
    text-indent: 30px;
    line-height: 20px;
    width: 100%;
    -webkit-appearance: none;
    outline: none !implode;
}
.form-col select {
    background-position: right 0px top 10px;
    background-image: url(https://leightonlaw.com/wp-content/themes/leighton/images/down-arrow-select.png);
    -webkit-appearance: none;
    background-repeat: no-repeat;
}

.get-started .form-col.col-textarea .wpcf7-form-control-wrap {
    display: block;
}
.get-started .form-submit-footer-btn input.wpcf7-submit {
    line-height: 24px !important;
    height: inherit !important;
    background: #FE680D !important;
    padding: 13px 0 16px!important;
    transition: 0.3s all;
    -webkit-transition: 0.3s all;
    text-decoration: none;
    border: none;
    color: #ffffff;
    font-family: Oswald;
    font-size: 24px;
    font-weight: 500;
    width: 214px;
    cursor: pointer;
}
.get-started .wpcf7-form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #FFF;
}
.get-started .wpcf7-form-control::-moz-placeholder { /* Firefox 19+ */
  color: #FFF;
}
.get-started .wpcf7-form-control:-ms-input-placeholder { /* IE 10+ */
  color: #FFF;
}
.get-started .wpcf7-form-control:-moz-placeholder { /* Firefox 18- */
  color: #FFF;
}
.blog .footer {
  background: none;
}
.content_ab {
    width: 100%;
    float: left;
    margin-top: -114px;
    background: #eaeae8;
}
.about_wrap1 {
  background: none;
}

.gap_news, .newPageContent, .content_ab {
  padding-top: 20px;
}

/*.newPageContent, .content_ab {
  padding-left: 10px;
  padding-right: 10px;
}*/

  .previous-button a, .next-button a {
      width: 120px;
      display: inline-block;
  }

.get-started .form-col .type-service:before {
  width: 20px;
  height: 20px;
  background-size: contain;
}

.nice-select {
  float: inherit;
  background-color: transparent;
  padding-right: 50px;
  border: none;
  max-width: 250px;
  border-bottom: solid 2px #fff;
  display: initial;
  border-radius: 0;
  padding-left: 0px;
}

.nice-select span.current {
  font-size:32px;
}

.nice-select span {
  font-weight: 900;
}

.nice-select:after {
  border-bottom: 3px solid #fff;
  border-right: 3px solid #fff;
  margin-top: -20px;
  height: 15px;
  width: 15px;
}

.nice-select.open ul {
  color: black;
  border-radius: 0;
}
.img_warp .image_linktop {
	position: absolute;
	height: 100%;
	width: 100%;
	left: 0;
	top: 0;
}
.type-service option {
    color: #000;
}





form#filter {
    display: inline-block;
}
.loadre_icon {
  display: none;
}
.category_button {
  display: none;
}
.cat_filter {
  display: none;
}
.loadre_icon {
    text-align: center;
    position: absolute;
    top: 50%;
    left: 0;
    right: 0;
}
.loadre_icon img {
  width: 10%;
}  
.left-content {
  position: relative;
}
.cat_filter {
  text-align: center;
}
.cat_filter p{
      cursor: pointer;
    background: #ff670e;
    display: inline-block;
    padding: 12px 60px;
    border-radius: 2px;
    color: #FFFF;
    font-family: Oswald;
    font-weight: 300;
    letter-spacing: 0.5px;
}


/****new search ****/
.search121 {
    display: flex;
    align-items: center;
}

.search121 input#keyword {
    background: none;
    border: none;
    border-bottom: 1px solid #FFF;
    color: #FFF;
    font-size: 31px;
    font-weight: 900 !important;
    margin-left: 10px;
    padding-right: 40px;
}

.newTemplatePageForm h2 {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
}
.content_ab.gap_news.newPageContent {
   min-height: 500px;
}
form#filter_search {
      position: relative;
}
.search_icon {
          position: absolute;
    right: 5px;
    top: 10px;
}
.search_icon img {
  max-width: 28px;
}  
input.click_serch {
         width: 28px;
    height: 28px;
    opacity: 0;
    position: absolute;
    top: 0;
    cursor: pointer;
    right: 0px;
}
@media only screen and  (min-width: 600px){
    .newTemplatePageForm h2 br {
        display: none;
    }
}  
@media only screen and  (max-width: 1024px){
  .get-started .mid_cont1 {
      width: 767px;
  }
  .get-started .form-col input, .form-col textarea, .form-col select {
    padding-bottom: 20px;
  }  
  .get-started .form-col {
    margin-bottom: 10px;
    max-width: 50%;
    flex: 0 0 50%;
    padding: 0 10px;
  }
  textarea.wpcf7-form-control.wpcf7-textarea {
      min-height: 125px;
  }

}  
@media only screen and  (max-width: 800px){
  .get-started .started-form {
       margin: 0; 
  }
  .get-started .form-col input, .form-col textarea, .form-col select {
    padding-bottom: 15px;
  } 
}  
@media only screen and  (max-width: 767px){
  .content_ab.gap_news.newPageContent {
      margin-top: -100px !important;
  }
}  
@media only screen and  (max-width: 600px){
  .search121 {
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .newTemplatePageForm h2 {
      display: block;
      align-items: center;
      justify-content: center;
      flex-wrap: wrap;
    }  
}  
@media only screen and  (max-width: 568px){

.newTemplatePageForm h2 {
    font-size: 25px;
    font-weight: 900 !important;
    font-family: Oswald;
}

.search121 input#keyword {
    background: none;
    border: none;
    border-bottom: 1px solid #FFF;
    color: #FFF;
    font-size: 25px;
    font-weight: 900 !important;
    margin-left: 10px;
}
.get-started .form-col input, .form-col textarea, .form-col select {
    padding-bottom: 8px;
}
.get-started .form-col {
    margin-bottom: 10px;
    max-width: 100%;
    flex: 0 0 100%;
    padding: 0 10px;
}
.get-started .mid_cont1 {
    max-width: 90%;
}
.get-started h2 {
    font-size: 30px;
}    
}


</style>
<?php

$cat_id = get_query_var('cat');
$image = get_field('category_header', 'category_'.$cat_id);
if($image){
?>
<div  class="headerImg" style="background-image:url(<?php echo $image; ?>);"></div>
<?php
}elseif(has_post_thumbnail()){
 //the_post_thumbnail('full');
 //the_post_thumbnail('full', array('class' => 'baner_i3'));
 $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" );
 ?><div class="headerImg" style="background-image:url(<?php 
    echo get_field("content_top_image", 7629);

 ?>);"></div> <?php
}else{ 
?>
<div  class="headerImg" style="background-image:url(<?php bloginfo('template_directory') ?>/images/about_banner.jpg)"></div>
<?php } ?>


  <?php if (have_posts()) : 
        ?>



  <div class="cur_wrap">

    <div class="about_wrap1">

      <div class="mid_cont1">

        <div class="content_ab gap_news newPageContent" >

          <div class="left-content">

            <article>

                <div id="response">
                </div>
                <div class="cat_filter loadmore_search"><p>LOAD MORE</p></div>

                <!-- <div class="cat_filter loadmore_cat"><p>LOAD MORE</p></div> -->


              <div class="loadre_icon"><img src="https://loading.io/spinners/wave/index.wave-ball-preloader.svg"></div>  

              <div style="" class="main_warpper">

                
                <?php /*
			 	           $args = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'posts_per_page' => '9',
                    'paged' => 1,
                );
                $my_posts = new WP_Query( $args ); */
                
                  if ( have_posts() ) : 

                    while ( have_posts() ) : the_post(); 
                       $img = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail' )[0];
                        $full_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' )[0];

                        ?>
                        <div class="main_wrap"> 
                          <?php if($img) {?>
                           <div class="img_warp" data-link="<?php echo $full_image; ?>" style="background: url(<?php echo $img; ?>); background-repeat: no-repeat; background-size: cover; max-width: 100%; max-height: 350px; height: 350px; background-position: center center;position:relative;">
                            <a href="<?php the_permalink(); ?>" class="image_linktop"></a>
							             </div>
                          <?php } ?>
                          <div class="blog_main_wrap">
                            <div class="blog_date"><?php the_date(); ?></div>
                              <div class="blog_wrap">
                                <h2><a href="<?php the_permalink(); ?>" title="Read more"><?php the_title(); ?></a></h2>
                                <?php the_excerpt(); ?>
                              </div>
                            </div>  
                          </div>

                    <?php endwhile; ?>

                    <?php //twentytwelve_content_nav( 'nav-below' ); ?>

                    <?php else : ?>

                        <?php echo "There is no post related to this category..." ?>

                    <?php endif; ?>

                <?php //wp_reset_query(); ?>

                </div>

              <?php //twentytwelve_content_nav( 'nav-below' ); ?>

            </article>

          </div>

        </div>

    <!-- <div class="loadmore load-more"><p>LOAD MORE</p></div> -->
  <div class="blog-navigation">

		 <?php wpbeginner_numeric_posts_nav(); ?>

		</div>

        <!-- <?php //include( "php/logo-bar.php" ); ?>

        <?php //get_sidebar( 'above' ); ?> -->

      </div>

    </div>

  </div>

  

  <?php else : ?>

  <div class="post">

    <h2 class="posttitle">

      <?php _e('Not Found') ?>

    </h2>

    <div class="postentry">

      <p>

        <?php _e('Sorry, no posts matched your criteria.'); ?>

      </p>

    </div>

  </div>

  <?php endif; ?>

</div>



<div class="get-started" style="background-image: url('<?php echo get_field('background_image', 7242);?>');">
  <div class="mid_cont1">
    <h2><?php echo get_field('form_title', 7242);?></h2>
    <?php echo do_shortcode(get_field('contact_form_shortcode', 7242));?>
  </div>
</div>






<script type="text/javascript">
$("#filter").on('change', '#categorydropdown', function() {
	jQuery( ".category_button" ).trigger( "click" );
});
  
  jQuery(function($){
  jQuery('#filter').submit(function(e){
     event.preventDefault(e);
    var filter = jQuery('#filter');
	 jQuery('#filter .nice-select').removeClass('open');
    jQuery.ajax({
      url:filter.attr('action'),
      data:filter.serialize(),
      type:filter.attr('method'),
      beforeSend:function(xhr){
		jQuery('#filter .nice-select').removeClass('open');
        filter.find('button').text('Processing...');
        jQuery(".main_warpper").css("display","none");
        jQuery(".loadre_icon").css("display","block");
	  },
      success:function(data){
		jQuery('#filter .nice-select').removeClass('open');
        filter.find('button').text('Apply filter');
        jQuery(".loadre_icon").css("display","none");
        jQuery(".load-more").css("display","none");
        jQuery(".cat_filter ").css("display","block");
        jQuery('#response').html(data);
	  }
    });
    return false;
  });
});

</script>


<?php get_footer(); ?>

