<?php
/*Template Name: News Page*/
get_header();
?>
<script type="text/javascript">
  $(document).ready(function(){

	  $(".toggle_cat h2").click(function()
	  {
			var openMe = $(this).next();
			//alert(openMe)
			var mySiblings = $(this).parent().siblings().find('ul');

			if (openMe.is(':visible')) {

				openMe.slideUp();
				$(this).parent().find("ul").slideUp();
				$(this).removeClass('down');

			} else {
				//
				mySiblings.slideUp();
				openMe.slideDown();
				$(".toggle_cat h2").removeClass('down');
				$(this).addClass('down');

			}

	  });

	     });

</script>
    <div class="cur_wrap">
        <div class="about_wrap1">
            <div class="mid_cont1">
                <div class="content_ab gap_news newPageContent">
					    <?php
					   $news_cat = array('mda-leighton-latest','mda-speeches','mda-media-kit','mda-press-office','mda-publications','mda-news-coverage','mda-seminars','mda-leighton-law-videos','mda-frequently-asked-questions','newsletters');
                       $i=0;
					   foreach($news_cat as $cat){
                            if($i==0){
                                echo '<a href="'.home_url().'/blog/" target="_blank"><div class="toggle_cat"><h2 class="up">Blog</h2></div></a>';
                                echo '<a href="https://leightonlaw.com/teleleighton/" target="_blank"><div class="toggle_cat"><h2 class="up">TeleLeighton</h2></div></a>';
                            }
						   $getcat = get_category_by_slug($cat);
						   echo '<div class="toggle_cat">';
						   echo '<h2 class="up">'.$getcat->name.'</h2>';
						   query_posts('post_type=post&category_name='.$cat.'&posts_per_page=-1&orderby=date&order=DESC');
						     if(have_posts()){
							      echo '<ul id="lcp_instance_0" class="lcp_catlist">';
    						      while(have_posts()){
                                      the_post();
        							echo '<li>';
        							echo '<a href="'.get_permalink().'">';
        							the_title();
        							echo '</a>';
        							echo '</li>';
        						   }
								echo '<ul>';
							 }
						   wp_reset_query();
						   echo '</div>';
                             $i++;
						}?>
                    <div class="clear"></div>
                </div>
                <?php include( "php/logo-bar.php" ); ?>
				<?php get_sidebar( 'above' ); ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>

<?php get_footer(); ?>
