<?php /* Template Name: home-page-new */ ?>
<?php
include('home-new-header.php'); ?>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script> -->
<script type="text/javascript">
	jQuery( document ).ready( function($) {
		$( "#menu-new-menu > li > ul > li" ).mouseenter( function() {
			var menu = $( this );
			$( ".sub-menu", menu ).css( { top: - menu.offset().top + $( "#menu-new-menu" ).offset().top + $( "#menu-new-menu > li" ).height() } );
			console.log( menu.offset().top );
		});
	});
</script>
<style type="text/css">
	.rc-anchor-error-msg-container {
		    display: none;
		}
	span.wpcf7-not-valid-tip {
		    color: red;
		    font-size: 1em;
		    font-weight: 400;
		    display: block;
		    top: 74px;
		    position: absolute;
		    left: 10px;
		}
	#wpcf7-f7991-p7242-o2 .wpcf7-form-control-wrap{
		 margin-bottom: 20px;
	}
	.consultation span.wpcf7-not-valid-tip {
	    color: red;
	    font-size: 10px;
	    left: 12px;
	    position: absolute;
	    top: 22px;
	    margin-top: 0!important;
	    font-weight: bold;
	}
	#mask {
		position:absolute;
		left:0;
		top:0;
		z-index:9000;
		background-color:#26262c;
		display:none;
		z-index: 100000;
		opacity: 0.6 !important;
	}
	#boxes .window {
		position:absolute;
		left:0;
		top:0;
		width:440px;
		height:850px;
		display:none;
		z-index:9999;
		padding:20px;
		border-radius: 5px;
		text-align: center;
	}
	#boxes #dialog {
		width:400px;
		height:auto;
		padding: 10px 10px 10px 10px;
		background-color:#ffffff;
		font-size: 15pt;
		z-index: 1000000;
		top: 10% !important;
	}

	.agree:hover{
		background-color: #D1D1D1;
	}
	.popupoption:hover{
		background-color:#D1D1D1;
		color: green;
	}
	.popupoption2:hover{
		color: red;
	}
	.red-box{
		background-color: #e12810;
		padding: 16px 15px;
		text-align: center;
		color: #fff;
	}
	.red-box p {
		font-family: 'Open Sans',sans-serif;
		font-weight: 600;
		font-size: 15px;
	}
	.new-sec {
		text-align: center;
		padding: 0 15px 50px;
		width: 100%;
	}
	.sec-sub {
		margin: 38px 0px;
	}
	.new-sec h1{
		font-family: Oswald;
		font-weight: 700;
		font-size: 28px;
		color: #000;
		margin-bottom: 15px;
	}

	#read{
		font-size: 16px;
		color: #fff;
		font-family: 'Oswald',sans-serif;
		font-weight: 300;
		width: auto;
		line-height: 24px;
		height: inherit;
		background: #fe680d;
		margin-left: 0;
		padding: 7px 33px;
		transition: .3s all;
		-webkit-transition: .3s all;
		text-decoration: none;
		cursor: pointer;
	}
	.red-cen{
		text-align: center;
	}
	@media (max-width: 991px){
		.banner-slider-home .banner-slider {
			position: relative;
			top: 0%;
		}
		.red-box p {
			font-size: 15px;
		}
	}

	@media (max-width: 767px){

		.red-box p {
			font-size: 12px;
		}
	}
</style>


<div class="red-box">
	<p>In accordance with CDC guidelines based on the Covid-19 pandemic, we offer free video consultation signups with an attorney and digital signup packets.</p>
</div>
<div class="cur_wrap">
	<div id="content">
		<div class="mid_cont1">
			<div class="cur_sli">
				<?php include( "php/logo-bar-newhome.php" ); ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="sero_cont home-column">
			<div class="left-column">
				<a href="#video-popup" class="fancybox"><img src="<?php echo get_field('left_section_image');?>"></a>
				<div class="img-bottom">
					<p><?php echo get_field('bottom_ipad_text');?></p>
					<a href="<?php echo get_field('ipad_video_url');?>" target="_blank"><?php echo get_field('ipad_video_text');?></a>
				</div>
			</div>
			<div class="right-column">
				<?php the_post(); the_content(); ?>
			</div>
		</div>

		<div class="clear"></div>

		<div class="mid_cont1">
			<?php get_sidebar( 'above' ); ?>
		</div>


		<div class="clear"></div>

		<!-- <div class="mid_cont1">
			 <div class="started-form">
			<div class="new-sec">
				<h1>Our Thoughts on the
					Situation We're All Facing.
				</h1>

				<div class="sec-sub">
				<img src="/wp-content/uploads/2009/07/covid-graphic.jpg">
			</div>
			<div class="red-cen">
				<a id="read">Close</a>
			</div>
			</div>
		</div> -->
	</div>

	<div class="get-started" style="background-image: url('<?php echo get_field('background_image');?>');">
		<div class="mid_cont1">
			<h2><?php echo get_field('form_title');?></h2>
			<?php echo do_shortcode(get_field('contact_form_shortcode'));?>
		</div>

	</div>
</div>
<div class="clear"></div>

</div>
<div class="video-popup" id="video-popup" style="display: none;">
	<?php echo get_field('popup_video_iframe');?>
</div>


<?php include('footer-home.php'); ?>
