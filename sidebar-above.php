<div class="wrap_watch clearboth ddd" style="margin-top: 45px;">
	<div class="iner_wat gap1">
		<a class="box-img" href="<?php echo home_url();?>/john-leighton-discusses-parasailing-accident-and-lack-of-regulation-on-cnn/"><img src="<?php bloginfo( "template_directory" ); ?>/images/video_first.png" alt="video" /></a>
		<div class="equal-text"><h4>Parasailing safety regulations.</h4><p>John Leighton appears on CNN to comment on proposed parasailing safety regulations.</p></div>
		<a class="box-btn" href="<?php echo home_url();?>/john-leighton-discusses-parasailing-accident-and-lack-of-regulation-on-cnn/">Read More</a>
	</div>
	<div class="iner_wat gap2">
		<a class="box-img" href="<?php echo home_url();?>/todayshow-com-family-of-parasailing-victim-pushes-for-law-sept-12-2007/"><img src="<?php bloginfo( "template_directory" ); ?>/images/video-second.png" alt="video" /></a>
		<div class="equal-text"><h4>Today Show with John</h4><p>John Leighton appears on NBC’s “Today” show as part of his representation of the family of Amber May White, who was killed in a parasailing tragedy.</p></div>
		<a class="box-btn" href="<?php echo home_url();?>/todayshow-com-family-of-parasailing-victim-pushes-for-law-sept-12-2007/">Read More</a>
	</div>
	<div class="iner_wat gap2">
		<a class="box-img" target='_blank' href="<?php echo home_url();?>/wp-content/uploads/2018/03/Leighton-profile-SFLG-Miami.jpg"><img src="<?php bloginfo( "template_directory" ); ?>/images/video-third.png" alt="video" /></a>
		<div class="equal-text"><h4>Cover story: Leighton Crusader for Safety</h4><p>John Leighton featured on the cover of South Florida Legal Guide</p></div>
		<a target='_blank' class="box-btn" href="<?php echo home_url();?>/wp-content/uploads/2018/03/Leighton-profile-SFLG-Miami.jpg">Read More</a>
	</div>
	<div class="iner_wat gap4">
		<a class="box-img" target='_blank' href="http://legalsolutions.thomsonreuters.com/law-products/Practice-Materials/Litigating-Premises-Security-Cases-AAJ-Press/p/100004681"><img src="<?php bloginfo( "template_directory" ); ?>/images/video-four.jpg" alt="video" /></a>
		<div class="equal-text"><h4>Publications</h4><p>John Leighton’s 2 volume book <em>Litigating Premises Security Cases.</em></p></div>
		<a target='_blank' class="box-btn" href="http://legalsolutions.thomsonreuters.com/law-products/Practice-Materials/Litigating-Premises-Security-Cases-AAJ-Press/p/100004681">Read More</a>
	</div>
</div>   